define pattern year="[0-9][0-9][0-9][0-9]"
do (a)year-.-(b)content(f)-.-(c)$until[-,$startwith"Posted by"](e);(d)-(e)..$l("title",(b),(f)).$l("text",(c),(d)).$l("_new",(b),(d))
do (a).$to.text.(b).(c)link(d).$l("li",(d))$l("_li",(a),(d))
set article=_new[title!,text,link?=_li.li]
output article
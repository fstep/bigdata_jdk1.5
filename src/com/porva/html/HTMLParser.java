/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.html;

import java.net.URI;
import java.util.List;

import com.porva.crawler.service.WorkerService;

public interface HTMLParser extends WorkerService
{
  /**
   * SESQ HTML to XML parser.
   */
  public static final String SESQ = "parser.sesq";

  public String process(final String html, final URI uri);
  
  /**
   * Returns title of the last parsed document.
   * 
   * @return title of the last parsed document or <code>null</code> if no title available or
   *         parsed did not tried to extract it.
   */
  public String getTitle();

  /**
   * Returs list of links of the last parsed document.
   * 
   * @return list of links of the last parsed document or <code>null</code> if links are not
   *         available or parser did not tried to extract them.
   */
  public List<String> getLinks();

}

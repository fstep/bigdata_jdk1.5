/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html;

import java.io.StringWriter;
import java.net.URI;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;

import com.porva.crawler.CrawlerInit;
import com.porva.crawler.service.AbstractWorkerService;
import com.porva.crawler.service.ServiceException;
import com.porva.crawler.service.WorkerService;
import com.porva.html.keycontent.Kce;
import com.porva.html.keycontent.KceSettings;
import com.porva.html.keycontent.LinkFoundListener;
import com.porva.html.keycontent.TitleFoundListener;

public class KCEParser extends AbstractWorkerService implements DOMDocumentParser
{
  private static final KceSettings settings = new KceSettings();

  private final Kce extractor;

  private final LinkFoundListener linkFoundListener;

  private final TitleFoundListener titleNodeFoundListener;

  private Document document = null;
  
  private String kceEncoding = "utf-8";

  private static final Log logger = LogFactory.getLog(KCEParser.class);

  public KCEParser()
  {
    super(DOMDocumentParser.KCE);

    linkFoundListener = new LinkFoundListener();
    titleNodeFoundListener = new TitleFoundListener();
    extractor = new Kce(settings);
    extractor.registerNodeFoundListener(linkFoundListener);
    extractor.registerNodeFoundListener(titleNodeFoundListener);
    
    if (CrawlerInit.getProperties().get("parser.kce.output-encoding") != null)
      kceEncoding = (String)CrawlerInit.getProperties().get("parser.kce.output-encoding");

  }
  
  public String process(final String doc, final URI uri)
  {
    throw new NotImplementedException(); // FIXME
  }

  public String process(final Document doc, final URI uri)
  {
    if (doc == null)
      throw new NullArgumentException("doc");

    document = extractor.extractKeyContent(doc, uri);
    if (document == null) { // parser failed
      return null;
    }

    // KeyContentExtractor.removeTheSameLinks(linkFoundListener.getFoundNodes(), uri.toString());
    StringWriter stringWriter = new StringWriter();
    Kce.prettyPrint(document, kceEncoding, stringWriter);

    return stringWriter.toString();
  }

  public String getTitle()
  {
    return titleNodeFoundListener.getTitle();
  }

  public List<String> getLinks()
  {
    return linkFoundListener.getLinks();
  }

  public WorkerService newCopyInstance() throws ServiceException
  {
    return new KCEParser();
  }

}

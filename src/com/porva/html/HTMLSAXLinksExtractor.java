/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.html;

import static com.porva.crawler.task.FetchResult.Status.COMPLETE;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;

import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.net.MalformedCrawlerURLException;
import com.porva.crawler.service.AbstractWorkerService;
import com.porva.crawler.service.LinksExtractorService;
import com.porva.crawler.service.WorkerService;
import com.porva.crawler.task.FetchResult;

/**
 * Extracts links from HTML pages using HTML SAX parser.
 * 
 * @author Poroshin V.
 * @date Oct 14, 2005
 */
public class HTMLSAXLinksExtractor extends AbstractWorkerService implements LinksExtractorService
{
  private HTMLSAXParser parser = new HTMLSAXParser();

  private static final Log logger = LogFactory.getLog(HTMLSAXLinksExtractor.class);

  /**
   * Constructs a new instance of {@link HTMLSAXLinksExtractor}.
   */
  public HTMLSAXLinksExtractor()
  {
    super("workerservice.linksExtractor");
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.service.LinksExtractorService#extractLinks(com.porva.crawler.task.DefaultFetchResult)
   */
  public int extractLinks(final FetchResult fetchTaskResult, final List<CrawlerURL> listBuff)
  {
    if (fetchTaskResult == null)
      throw new NullArgumentException("fetchTaskResult");
    if (listBuff == null)
      throw new NullArgumentException("listBuff");

    int extractedNum = 0;

    if ((fetchTaskResult.getStatus() == COMPLETE)
        && (fetchTaskResult.getResponse().getCode() == 200)) {
      extractedNum = extractLinks(fetchTaskResult.getTask().getCrawlerURL(), fetchTaskResult
          .getResponse().getBody(), listBuff);
    }

    return extractedNum;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.service.LinksExtractorService#extractLinks(com.porva.crawler.net.CrawlerURL,
   *      byte[], java.util.List)
   */
  public int extractLinks(final CrawlerURL crawlerURL,
                          final byte[] content,
                          final List<CrawlerURL> listBuff)
  {
    if (crawlerURL == null)
      throw new NullArgumentException("crawlerURL");
    if (content == null)
      throw new NullArgumentException("content");
    if (listBuff == null)
      throw new NullArgumentException("listBuff");

    LinkContentHandler handler = new LinkContentHandler();
    parser.setContentHandler(handler);
    try {
      parser.parse(new InputSource(new ByteArrayInputStream(content)));
    } catch (Exception e) {
      logger.warn("Extraction links error: ", e);
    }

    List<String> links = handler.getLinks();

    CrawlerURL baseURL = handler.getBaseURL(crawlerURL.getDepth());
    if (baseURL != null)
      return resolveLinks(baseURL, links, listBuff);

    return resolveLinks(crawlerURL, links, listBuff);
  }
  
  public int resolveLinks(final CrawlerURL baseURL,
                           final List<String> links,
                           final List<CrawlerURL> listBuff)
  {
    if (baseURL == null)
      throw new NullArgumentException("baseURL");
    if (links == null)
      throw new NullArgumentException("links");
    if (listBuff == null)
      throw new NullArgumentException("listBuff");

    int resolvedNum = 0;

    for (String linkStr : links) {
      try {
        CrawlerURL link = baseURL.resolveAsChild(linkStr);
        listBuff.add(link);
        resolvedNum++;
      } catch (MalformedCrawlerURLException e) {
        if (logger.isDebugEnabled())
          logger.debug("Failed to resolve link  " + linkStr + " :" + e.getCause());
      }
    }

    return resolvedNum;
  }

  public WorkerService newCopyInstance()
  {
    return new HTMLSAXLinksExtractor();
  }

  // ///////////////////////////////////////////////////////////////////////

  private static class LinkContentHandler extends DefaultHandler
  {
    private List<String> links = new ArrayList<String>();

    private String baseURL = null;

    /**
     * Returns list of found links.
     * 
     * @return list of found links.
     */
    public List<String> getLinks()
    {
      return links;
    }

    /**
     * Returns base crawler url, i.e. url in <base href="..."> tag; or <code>null</code> if there
     * is no such tag or 'href' localtion contains ivalid url.
     * 
     * @return base crawler url or <code>null</code> if it is impossible to extract it.
     */
    public CrawlerURL getBaseURL(int depth)
    {
      CrawlerURL crawlerBaseURL = null;
      if (baseURL != null) {
        try {
          crawlerBaseURL = new CrawlerURL(depth, baseURL);
        } catch (MalformedCrawlerURLException e) {
          logger.warn("Failed to construct base url: " + baseURL);
        }
      }
      return crawlerBaseURL;
    }

    public void startElement(String uri, String name, String qName, Attributes atts)
    {
      if (qName.equals("A") || name.equals("A")) {
        String link = atts.getValue("href");
        if (link != null) {
          links.add(link);
        }
      } else if (qName.equals("BASE") || name.equals("BASE")) {
        baseURL = atts.getValue("href");
      }
    }
  }

}

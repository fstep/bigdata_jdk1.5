/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.util.log;

import java.util.logging.*;

public class ConsoleFormatter extends Formatter
{

  public ConsoleFormatter()
  {
  }


  /**
   This method formats the given log record, in a java properties file style
   */
  public synchronized String format(LogRecord record)
  {
    StringBuffer buffer = new StringBuffer(50);
    buffer.append(record.getLevel().toString());
    buffer.append("\t");
    buffer.append(record.getMessage());
    buffer.append("\n");
    return buffer.toString();
  }
}

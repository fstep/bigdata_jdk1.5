/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.util.thread;

/**
 * Status of {@link WorkerThread}. <br><br>
 * Status diagram for {@link WorkerThread}<br>
 * <img src="../../../../pic/WorkerThread_statusdiagram.gif">
 */

public class WorkerThreadStatus
{
  private final String status;

  /**
   * {@link WorkerThread} thread is ready to start.
   */
  public static final WorkerThreadStatus READY = new WorkerThreadStatus("READY");

  /**
   * {@link WorkerThread} thread is running.
   */
  public static final WorkerThreadStatus RUNNING = new WorkerThreadStatus("RUNNING");

  /**
   * {@link WorkerThread} thread is scheduled to pauseWork.
   */
  public static final WorkerThreadStatus GONNA_PAUSE = new WorkerThreadStatus("PAUSING");

  /**
   * {@link WorkerThread} thread is paused.
   */
  public static final WorkerThreadStatus PAUSED = new WorkerThreadStatus("PAUSED");

  /**
   * {@link WorkerThread} thread is scheduled to sleep.
   */
  public static final WorkerThreadStatus GONNA_SLEEP = new WorkerThreadStatus("GONNA_SLEEP");

  /**
   * {@link WorkerThread} thread is sleeping.
   */
  public static final WorkerThreadStatus SLEEPING = new WorkerThreadStatus("SLEEPING");

  /**
   * {@link WorkerThread} thread is scheduled to finish it's job.
   */
  public static final WorkerThreadStatus GONNA_DONE = new WorkerThreadStatus("GONNA_DONE");

  /**
   * {@link WorkerThread} thread has done it's work.
   */
  public static final WorkerThreadStatus DONE = new WorkerThreadStatus("DONE");

  private WorkerThreadStatus(String status)
  {
    this.status = status;
  }

  public final String toString()
  {
    return new String(status);
  }

}

/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.thread;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.Crawler;
import com.porva.crawler.Frontier;
import com.porva.crawler.service.ServiceException;
import com.porva.crawler.service.ServiceProvider;
import com.porva.crawler.task.Task;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.builder.ToStringBuilder;

public class WorkersFixedThreadPool
{

  public enum State
  {
    GONNA_RUN,
    RUNNING,
    GONNA_PAUSE,
    PAUSED,
    GONNA_DONE,
    DONE,
    SLEEPING
  };

  private State state;

  private int poolSize;

  private Crawler crawler;

  private int pausedWorkers = 0;

  private int sleepingWorkers = 0;

  private int doneWorkers = 0;

  private static final Log logger = LogFactory.getLog(WorkersFixedThreadPool.class);

  /**
   * The threads of the pool.
   */
  private Thread workerThreads[] = null;

  /**
   * Waiting tasks list.
   */
  List<Task> assignments = new LinkedList<Task>();

  /**
   * The constructor.
   * 
   * @param poolSize number of threads in the thread pool.
   * @throws ServiceException
   */
  public WorkersFixedThreadPool(final int poolSize,
                                final Crawler crawler,
                                final Frontier frontier,
                                final ServiceProvider services) throws ServiceException
  {
    this.poolSize = poolSize;
    this.crawler = crawler;

    if (this.poolSize < 0)
      throw new IllegalArgumentException("poolSize cannot be < 0: " + this.poolSize);
    if (this.crawler == null)
      throw new NullArgumentException("crawler");
    if (frontier == null)
      throw new NullArgumentException("frontier");
    if (services == null)
      throw new NullArgumentException("services");

    workerThreads = new CrawlerWorker[poolSize];
    for (int i = 0; i < workerThreads.length; i++) {
      workerThreads[i] = new CrawlerWorker(this, frontier, services);
      workerThreads[i].start();
    }
  }

  private void setPoolState(final State newState)
  {
    this.state = newState;
    crawler.workersPoolStateChanged(newState);
    logger.debug("WorkerThreadPool state changed: " + this.state);
    notifyAll();
  }

  public State getPoolState()
  {
    return state;
  }

  /**
   * Add a task to the thread pool. Any class which implements the Runnable interface may be
   * assienged. When this task runs, its run method will be called.
   * 
   * @param task An object that implements the Runnable interface
   */
  public synchronized void assign(Task task)
  {
    assignments.add(task);
    notify();
  }

  /**
   * Returns a new Task to perform by crawler worker or <code>null</code> if no more tasks will be
   * available.
   * 
   * @return a new Task to perform by crawler worker or <code>null</code> if no more tasks will be
   *         available.
   */
  synchronized Task getAssignment()
  {
    try {
      if (state == State.GONNA_PAUSE) {
        pauseWorker();
      }
      if (state == State.GONNA_DONE) {
        return null;
      }

      if (!assignments.iterator().hasNext()) {
        sleepingWorkers++;
        if (sleepingWorkers == poolSize)
          setPoolState(State.SLEEPING);
        while (!assignments.iterator().hasNext()) {
          wait(); // SLEEPING
          if (state == State.GONNA_PAUSE)
            pauseWorker();
          if (state == State.GONNA_DONE) {
            return null;
          }

        }
        sleepingWorkers--;
      }

    } catch (InterruptedException e) {
      logger.fatal(e);
      e.printStackTrace();
    }

    if (state != State.RUNNING)
      setPoolState(State.RUNNING);

    Task task = assignments.iterator().next();
    assignments.remove(task);
    return task;
  }

  synchronized void workerDone()
  {
    doneWorkers++;
    if (doneWorkers == poolSize)
      setPoolState(State.DONE);
  }

  private synchronized void pauseWorker() throws InterruptedException
  {
    pausedWorkers++;
    // logger.debug("paused workers " + pausedWorkers);

    if (pausedWorkers == poolSize)
      setPoolState(State.PAUSED);
    while (state == State.GONNA_PAUSE || state == State.PAUSED)
      wait();
    pausedWorkers--;
    // logger.debug("paused workers " + pausedWorkers);
  }

  public synchronized void pause()
  {
    setPoolState(State.GONNA_PAUSE);
  }

  public synchronized void resume()
  {
    setPoolState(State.GONNA_RUN);
  }

  public synchronized void stop()
  {
    assignments.clear(); // todo
    setPoolState(State.GONNA_DONE);
  }

  public synchronized void waitUntil(State state) throws InterruptedException
  {
    while (this.state != state)
      wait();
  }

  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("state", state)
        .append("poolSize", poolSize).append("pausedWorkers", pausedWorkers)
        .append("sleepingWorkers", sleepingWorkers).append("doneWorkers", doneWorkers).toString();
  }
  
}

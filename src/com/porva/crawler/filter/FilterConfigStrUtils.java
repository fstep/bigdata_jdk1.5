/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.filter;

import org.apache.commons.lang.NullArgumentException;

public class FilterConfigStrUtils
{
  private FilterConfigStrUtils()
  {
  }

  public static ConfigParam parseBooleanAndStringListStr(final String configStr,
                                                         boolean isToLowCase,
                                                         final String paramCheckPattern)
  {
    if (configStr == null)
      throw new NullArgumentException("configStr");
    if (configStr.trim().equals(""))
      throw new IllegalArgumentException("configStr cannot be empty");

    String str = configStr;
    boolean isAllow = true;

    if (isToLowCase)
      str = str.toLowerCase();
    if (str.startsWith("allow:")) {
      str = str.substring(6, str.length());
      isAllow = true;
    } else if (str.startsWith("disallow:")) {
      str = str.substring(9, str.length());
      isAllow = false;
    }

    if (str == null || str.trim().equals("")) {
      return new ConfigParam(isAllow, null);
    }

    String[] params = str.split(",");
    if (paramCheckPattern != null) {
      for (String param : params) {
        if (!param.matches(paramCheckPattern))
          throw new IllegalArgumentException("Invalid parameter " + param + " in config string: "
              + configStr);
      }
    }

    return new ConfigParam(isAllow, params);
  }

  // /////////////////////////////////////////////////////////
  static class ConfigParam
  {
    private boolean isAllow = true;

    private String[] params;

    ConfigParam(boolean isAllow, String[] list)
    {
      this.isAllow = isAllow;
      this.params = list;
    }

    public boolean isAllow()
    {
      return isAllow;
    }

    public String[] getParams()
    {
      return params;
    }
  }
}

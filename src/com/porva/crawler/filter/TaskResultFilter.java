/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.filter;

import com.porva.crawler.task.TaskResult;

/**
 * Filter interface that validates {@link TaskResult} objects.
 * 
 * @author Poroshin V.
 * @date Nov 17, 2005
 */
public interface TaskResultFilter extends Filter
{
  /**
   * Validates given <code>taskResult</code> according to this filter.
   * 
   * @param taskResult
   * @return <code>true</code> if <code>taskResult</code> is valid; <code>false</code>
   *         otherwise.
   */
  public boolean isValid(final TaskResult taskResult);

  /**
   * Returns <code>true</code> if given <code>taskResult</code> is valid to generate other
   * tasks.
   * 
   * @param taskResult {@link TaskResult} object to validate.
   * @return <code>true</code> if given <code>taskResult</code> is valid to generate other
   *         tasks; <code>false</code> otherwise.
   */
  public boolean isValidToProduce(final TaskResult taskResult);
}

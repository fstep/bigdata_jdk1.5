/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.filter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.filter.FilterConfigStrUtils.ConfigParam;
import com.porva.crawler.service.ServiceException;
import com.porva.crawler.service.WorkerService;
import com.porva.crawler.task.FetchTask;
import com.porva.crawler.task.Task;

/**
 * URI filter of file extensions.<br>
 * This filter extracts filename from URI and checks its extension agains list of valid extensions.
 * If filename in URI cannot be retrieved in cases like "http://server.com/", "http://server.com" or
 * http://server.com/path then these URIs are treated as ended with html file extension.
 * 
 * @see #isValid(FetchTask)
 */
public class FileTypeFilter extends AbstractTaskFilter
{
  private String[] fileTypes = null;

  private boolean isAllow = true;

  private Pattern filePattern = Pattern.compile(".*/[^/]+\\.([^/]+)$");
  
  /**
   * Allocates new {@link FileTypeFilter} objct with list of allowed file extensions.
   * @param fileTypes list of allowed extensions.
   * 
   * @throws Exception if <code>validFileTypes</code> is empty.
   * @throws NullArgumentException if <code>fileTypes</code> is <code>null</code>.
   * @throws IllegalArgumentException if <code>fileTypes</code> is empty.
   */
  FileTypeFilter(boolean isAllow, String[] fileTypes)
  {
    super(Filter.FILE_TYPE, ProcessingSpeed.VERY_FAST);
    init(isAllow, fileTypes);
  }

  /**
   * Init the filter.<br>
   * @param fileTypes array of allowed file types.
   * 
   * @throws NullArgumentException if <code>fileTypes</code> is <code>null</code>.
   * @throws IllegalArgumentException if <code>fileTypes</code> is empty.
   */
  void init(boolean isAllow, final String[] fileTypes)
  {
    this.isAllow = isAllow;
    this.fileTypes = (String[]) ArrayUtils.clone(fileTypes);
    checkFileTypes();
  }
  
  private void checkFileTypes()
  {
    if (this.fileTypes == null)
      throw new NullArgumentException("fileTypes");
    if (this.fileTypes.length == 0)
      throw new IllegalArgumentException("None of file types is specified");

    for (String fileType : fileTypes) 
      if (fileType.trim().equals(""))
        throw new IllegalArgumentException("Invalid file type: file type cannot be an empty string.");
  }

  /**
   * Tests if given {@link FetchTask} object is valid to proceed according to defined valid file
   * types. <br>
   * Note that URIs like "http://server.com/" or "http://server.com" or http://server.com/path are
   * treated as html files.
   * 
   * @param task task to check.
   * @return <code>true</code> if task has passed the filer; <code>false</code> otherwise.
   * @throws NullArgumentException if <code>task</code> is <code>null</code>.
   */
  public boolean isValid(final Task task)
  {
    if (task == null)
      throw new NullArgumentException("task");

    if (!(task instanceof FetchTask))
      return true;
    FetchTask fetchTask = (FetchTask) task;

    String path = fetchTask.getCrawlerURL().getPath();
    String fileType = null;
    if (path == null || path.equals("")) // todo: is it so that null path is equivalent to empty
      path = "/";

    Matcher matcher = filePattern.matcher(path);
    if (matcher.matches()) {
      fileType = matcher.group(1);
      fileType = fileType.toLowerCase();
    } else
      fileType = "html"; // for links like "http://server.com/" or "http://server.com" or
    // http://server.com/path

    for (int i = 0; i < fileTypes.length; i++)
      if (fileTypes[i].equals(fileType))
        return isAllow;

    // if (!isAllow && logger.isLoggable(Level.INFO))
    // logger.info("Rejected by FileTypesFilter: url=" + ((FetchTask)task).getURIString());

    return !isAllow;
  }

  public boolean isValidToProduce(Task task)
  {
    return true;
  }

  /**
   * Returns list of allowed file types as comma separated string. This returned string can be used
   * to construct new {@link FileTypeFilter} object.
   * 
   * @return string of allowed file types.
   */
  public String getConfigStr()
  {
    StringBuffer configStr = new StringBuffer();
    if (isAllow)
      configStr.append("allow:");
    else
      configStr.append("disallow:");
    for (int i = 0; i < fileTypes.length; i++)
      configStr.append(fileTypes[i]).append(",");

    return configStr.toString().substring(0, configStr.length() - 1);
  }

  public WorkerService newCopyInstance() throws ServiceException
  {
    return new FileTypeFilter(isAllow, fileTypes);
  }

}

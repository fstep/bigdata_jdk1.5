/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.filter;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.porva.crawler.service.ServiceException;
import com.porva.crawler.service.WorkerService;
import com.porva.crawler.task.FetchTask;
import com.porva.crawler.task.Task;

/**
 * Filter that limits number of redirects that the crawler can follow.
 *
 * @author Poroshin V.
 */
class RedirectsLimitFilter extends AbstractTaskFilter
{
  
  private int maxRedirectesNum;
  
  /**
   * Creates a new {@link TaskFilter} that restricts number of redirects.
   * 
   * @param maxRedirectesNum maximum number of redirects.
   * @throws IllegalArgumentException if <code>maxRedirectesNum</code> is < 0.
   */
  public RedirectsLimitFilter(int maxRedirectesNum)
  {
    super(Filter.REDIRECTS_LIMIT, ProcessingSpeed.VERY_FAST);
    this.maxRedirectesNum = maxRedirectesNum;
    
    if (this.maxRedirectesNum < 0)
      throw new IllegalArgumentException("maxRedirectesNum must be >= 0: " + this.maxRedirectesNum);
  }

  /**
   * Returns <code>true</code> if given <code>task</code> is an instance of {@link FetchTask}
   * and it has crawler url redirects number less or equal then max. allowed redirects num.<br>
   * If given <code>task</code> is NOT an instance of {@link FetchTask} then <code>true</code>
   * will be returned.
   * 
   * @param task fetch task to validate.
   * @return <code>true</code> if <code>task</code> is valid according to this filter;
   *         <code>false</code> otherwise.
   * @throws NullArgumentException if <code>task</code> is <code>null</code>.
   */
  public boolean isValid(final Task task)
  {
    if (task == null)
      throw new NullArgumentException("task");

    if (!(task instanceof FetchTask))
      return true;

    FetchTask fetchTask = (FetchTask) task;

    return (fetchTask.getCrawlerURL().getRedirNum() <= maxRedirectesNum);
  }

  /* (non-Javadoc)
   * @see com.porva.crawler.service.WorkerService#newCopyInstance()
   */
  public WorkerService newCopyInstance() throws ServiceException
  {
    return new RedirectsLimitFilter(maxRedirectesNum);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("maxRedirectesNum",
                                                                                maxRedirectesNum)
        .toString();
  }

  public boolean isValidToProduce(Task task)
  {
    return true;
  }

  public String getConfigStr()
  {
    return String.valueOf(maxRedirectesNum);
  }
  
  
  
}

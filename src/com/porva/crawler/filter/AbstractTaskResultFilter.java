/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.filter;

/**
 * Default implementation of {@link TaskResultFilter} interface.
 * 
 * @author Poroshin V.
 */
public abstract class AbstractTaskResultFilter extends AbstractFilter implements TaskResultFilter
{

  /**
   * Allocates a new filter with specified service name and processing speed.
   * 
   * @param name name of the filter service.
   * @param processingSpeed estimated processing speed of the filter.
   */
  public AbstractTaskResultFilter(final String name, final ProcessingSpeed processingSpeed)
  {
    super(name, processingSpeed);
  }

}

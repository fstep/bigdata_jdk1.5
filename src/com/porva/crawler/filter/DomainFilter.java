/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.filter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.service.WorkerService;
import com.porva.crawler.task.FetchTask;
import com.porva.crawler.task.Task;

/**
 * {@link TaskFilter} implementation that allows or disallows specified domains to process.<br>
 * This filter has {@link com.porva.crawler.filter.Filter.ProcessingSpeed#VERY_FAST} processing
 * speed.
 * 
 * @author Poroshin V.
 * @date Sep 15, 2005
 */
class DomainFilter extends AbstractTaskFilter
{
  private String[] domains = null;

  private boolean isAllow = true;

  private static final Pattern p = Pattern.compile(".+\\.([^.]+)$");

  // ////////////////////////////////////
  // creation methods
  // ////////////////////////////////////
  /**
   * Creation method that constructs new {@link DomainFilter} object that will pass only allowed
   * domains specified by <code>allowedDomains</code>. If <code>allowedDomains</code> array is
   * <code>null</code> or empty then ALL domains will be disallowed except <code>null</code>
   * domain.<br>
   * <br>
   * URIs with empty domains are denoted as URIs with <code>null</code> domains. For URLs with
   * <code>null</code> domains {@link DomainFilter#isValid(CrawlerURL)} will always return
   * <code>true</code>.
   * 
   * @param allowedDomains array of allowed domains or <code>null</code> to disallow all domains.
   * @return new allocated {@link DomainFilter} object.
   */
  static DomainFilter newAllowDomainFilter(final String[] allowedDomains)
  {
    return new DomainFilter(true, allowedDomains);
  }

  /**
   * Creation method that constructs new {@link DomainFilter} object that will pass only domains NOT
   * specified by <code>disallowedDomains</code>. If <code>disallowedDomains</code> array is
   * <code>null</code> or empty then ALL domains will be allowed. <br>
   * URIs with empty domains are denoted as URIs with <code>null</code> domains. For URLs with
   * <code>null</code> domains {@link DomainFilter#isValid(CrawlerURL)} will always return
   * <code>true</code>.
   * 
   * @param disallowedDomains array of disallowed domains or <code>null</code> to allow all
   *          domains.
   * @return new allocated {@link DomainFilter} object.
   */
  static DomainFilter newDisallowDomainFilter(final String[] disallowedDomains)
  {
    return new DomainFilter(false, disallowedDomains);
  }
  
  void init(boolean isAllow, String[] domains)
  {
    this.domains = (String[]) ArrayUtils.clone(domains);
    this.isAllow = isAllow;
  }

  DomainFilter(boolean isAllow, String[] domains)
  {
    super(Filter.DOMAINS, ProcessingSpeed.VERY_FAST);
    init(isAllow, domains);
  }

  // ////////////////////////////////////
  // public methods
  // ////////////////////////////////////

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.filter.TaskFilter#isValidURI(com.porva.crawler.net.CrawlerURL)
   */
  public boolean isValid(final Task task)
  {
    if (task == null)
      throw new NullArgumentException("task");

    if (!(task instanceof FetchTask))
      return true;

    FetchTask fetchTask = (FetchTask) task;

    String domain = getDomain(fetchTask.getCrawlerURL().getHost());
    if (domain == null)
      return true;

    if (domains != null) {
      for (int i = 0; i < domains.length; i++) {
        if (domains[i].equalsIgnoreCase(domain))
          return isAllow;
      }
    }

    return !isAllow;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.service.WorkerService#newInstance()
   */
  public WorkerService newCopyInstance()
  {
    return new DomainFilter(isAllow, domains);
  }

  // ////////////////////////////////////
  // local and private methods
  // ////////////////////////////////////

  /**
   * Returns domain string of given host. If host is null or there is no domain specified then
   * <code>null</code> will be returned.
   * 
   * @return domain string of given host or <code>null</code> if
   *         <code>host<code> is <code>null</code> or 
   *         <code>host<code> does not have domain.
   */
  String getDomain(String host)
  {
    if (host == null)
      return null;

    Matcher matcher = p.matcher(host);
    if (matcher.matches()) {
      return matcher.group(1);
    }
    return null;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
        .appendSuper(super.toString()).append("domains", domains).append("isAllow", isAllow)
        .toString();
  }

  public boolean isValidToProduce(Task task)
  {
    return true;
  }

  public String getConfigStr()
  {
    // allow:org,net
    StringBuffer sb = new StringBuffer();
    if (isAllow)
      sb.append("allow:");
    else
      sb.append("disallow:");

    if (domains != null && domains.length != 0) {
      for (int i = 0; i < domains.length - 1; i++)
        sb.append(domains[i]).append(",");
      sb.append(domains[domains.length - 1]);
    }

    return sb.toString();
  }

}

/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.workload;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.Stat;
import com.porva.crawler.service.CommonService;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskResult;

/**
 * Interface of crawler's worload. Workload is a set of {@link Task} entries. Every task in workload
 * has one of the WorkloadEntryStatus status.
 * 
 * @author Poroshin V.
 * @date Sep 13, 2005
 */
public interface Workload extends Stat, CommonService
{

  /**
   * Returns next {@link Task} task with status {@link WorkloadEntryStatus#WAITING} and changes its
   * status to {@link WorkloadEntryStatus#RUNNING}.
   * 
   * @return {@link Task} workload entry. If there is no more tasks to process available in workload
   *         but there are still some running tasks then
   *         {@link com.porva.crawler.task.EmptyTask} will be returned. If there is no
   *         any tasks to process available and no running tasks then
   *         {@link com.porva.crawler.task.LastTask} instance will be returned.
   */
  public Task getTask();

  /**
   * Adds a new {@link Task} task to workload and sets is status as
   * {@link WorkloadEntryStatus#WAITING}.<br>
   * Only new tasks will be added to workload. It means that if <code>task</code> has any status
   * in workload then it will not be added to workload and method will return <code>false</code>.<br>
   * This method is equivalent to <code>addTask(Task, false)</code>.
   * 
   * @param task new tasks to add to workload.
   * @return <code>true</code> if task was added to workload; <code>false</code> otherwise.
   * @throws NullArgumentException if task is <code>null</code>.
   */
  public boolean addTask(Task task);

  /**
   * Adds a new {@link Task} task to workload and sets is status as
   * {@link WorkloadEntryStatus#WAITING}.<br>
   * <code>true</code> <code>isUpdate</code> parameter means that <code>task</code> should be
   * updated in workload and it will be processed as a new {@link WorkloadEntryStatus#WAITING} task.<br>
   * If <code>isUpdate</code> is <code>false</code> then only new tasks will be added to
   * workload. It means that if <code>task</code> has any status in workload then it will not be
   * added to workload and method will return <code>false</code>.
   * 
   * @param task new tasks to add to workload.
   * @param isUpdate <code>true</code> if task is for updating; <code>false</code> otherwise.
   * @return <code>true</code> if task was added to workload; <code>false</code> otherwise.
   * @throws NullArgumentException if task is <code>null</code>.
   */
  public boolean addTask(final Task task, boolean isUpdate);

  /**
   * Completes given task in workload by changing status of task from
   * {@link WorkloadEntryStatus#RUNNING} to {@link WorkloadEntryStatus#COMPLETE} if result is
   * complete or {@link WorkloadEntryStatus#FAILED} if result is failed.<br>
   * If given <code>task</code> is not present in workload or its status different from
   * {@link WorkloadEntryStatus#RUNNING} then this method will fail and return <code>false</code>.
   * 
   * @param task task to complete in workload.
   * @param result task's result.
   * @return <code>true</code> if task was competed in workload or <code>false</code> otherwise.
   * @throws NullArgumentException if <code>task</code> is <code>null</code>.
   * @throws NullArgumentException if <code>result</code> is <code>null</code>.
   */
  public boolean completeTask(Task task, TaskResult result);

  /**
   * Returns {@link WorkloadEntryStatus} status of the task in workload. If task is not present in
   * workload then {@link WorkloadEntryStatus#UNKNOWN} will be returned.
   * 
   * @param task {@link Task} object to get its status.
   * @return {@link WorkloadEntryStatus} status of the task
   * @throws NullArgumentException if <code>task</code> is <code>null</code>.
   */
  public WorkloadEntryStatus getTaskStatus(Task task);

  /**
   * Returns number of workload entries with given <code>status</code>.
   * 
   * @param status of entry in workload.
   * @return number of workload entries with given <code>status</code>.
   * @throws NullArgumentException if <code>status</code> is <code>null</code>.
   */
  public int getNumberOf(final WorkloadEntryStatus status);

  // /**
  // * Returns number of workload entries with status {@link WorkloadEntryStatus#RUNNING}.
  // *
  // * @return number of running entries in workload.
  // */
  // public int getRunningNum();
  //
  // /**
  // * Returns number of workload entries with status {@link WorkloadEntryStatus#WAITING}.
  // *
  // * @return number of waiting entries in workload.
  // */
  // public int getWaitingNum();
  //
  // /**
  // * Returns number of workload entries with status {@link WorkloadEntryStatus#COMPLETE}.
  // *
  // * @return number of compete entries in workload.
  // */
  // public int getCompleteNum();
  //
  // /**
  // * Returns number of workload entries with status {@link WorkloadEntryStatus#FAILED}.
  // *
  // * @return number of failed entries in workload.
  // */
  // public int getFailedNum();

}

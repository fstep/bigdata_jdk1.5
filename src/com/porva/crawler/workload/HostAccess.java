/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.workload;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.CrawlerInit;
import com.porva.crawler.db.DB;
import com.porva.crawler.db.DBFunctor;
import com.porva.crawler.task.FetchTask;
import com.porva.crawler.task.Task;

/**
 * Traks accesses to hosts in order to crawl politely.
 * 
 * @author Poroshin V.
 * @date Oct 10, 2005
 */
class HostAccess
{
  Map<String, Long> freeHosts = new HashMap<String, Long>();

  Map<String, Long> busyHosts = new HashMap<String, Long>();

  private DB<Task> hostTaskDB;

  private static final Long newTaskTime = new Long(0);
  
  private static final long SERVER_DELAY = Long.parseLong(CrawlerInit.getProperties().getProperty("crawler.server.delay"));

  private static Log logger = LogFactory.getLog(HostAccess.class);

  /**
   * Allocates a new instance of {@link HostAccess} object with specified database handler.<br>
   * 
   * @param hostTaskDB
   * @throws NullArgumentException if <code>hostTaskDB</code> is <code>null</code>.
   * @throws IllegalArgumentException if <code>hostTaskDB</code> is closed or not opened.
   */
  public HostAccess(final DB<Task> hostTaskDB)
  {
    this.hostTaskDB = hostTaskDB;
    if (this.hostTaskDB == null)
      throw new NullArgumentException("hostTaskDB");
    if (!this.hostTaskDB.isOpened())
      throw new IllegalArgumentException("Database handle is not opened.");
    
    if (logger.isInfoEnabled())
      logger.info("ServerDelay is " + SERVER_DELAY + " ms.");

    // init hostAccess by hostTaskDB
    DBFunctor<Task> initHostAccess = new DBFunctor<Task>()
    {
      public void process(String key, Task value)
      {
        if (key == null)
          throw new NullArgumentException("key");

        if (value instanceof FetchTask) {
          FetchTask fetchTask = (FetchTask) value;
          String host = fetchTask.getCrawlerURL().getHost();
          addHost(host);
        }
      }
    };
    this.hostTaskDB.iterateOverAll(initHostAccess);
  }

  /**
   * Marks given <code>host</code> hostname as a <i>free</i> if it is not already marked as free
   * or if it is not already marked as <i>busy</i>.<br>
   * <i>Free</i> hosts can be retrieved later using {@link HostAccess#getFreeHost()} function.<br> (<i>Free</i>
   * host means host that at least {@link CrawlerSettings#SERVER_DELAY} time elapsed after last
   * access to it.)
   * 
   * @param host hostname
   * @throws NullArgumentException if <code>host</code> is <code>null</code>.
   */
  public void addHost(final String host)
  {
    if (host == null)
      throw new NullArgumentException("host");

    if (!freeHosts.containsKey(host) && !busyHosts.containsKey(host))
      freeHost(host);
  }

  /**
   * Marks given <code>host</code> as <i>free</i>.
   * 
   * @param host hostname
   */
  public void freeHost(final String host)
  {
    if (host == null)
      throw new NullArgumentException("host");

    if (hostTaskDB.get(host) != null)
      freeHosts.put(host, newTaskTime);
  }

  /**
   * Returns some <i>free</i> host and mark it as <i>busy</i>.<br>
   * 
   * @return <i>free</i> host or <code>null</code> if there is no any.
   */
  public String getFreeHost()
  {
    update();
    String freeHost = null;
    Iterator<Map.Entry<String, Long>> it = freeHosts.entrySet().iterator();
    if (it.hasNext()) {
      Map.Entry<String, Long> entry = (Map.Entry<String, Long>) it.next();
      freeHost = entry.getKey();
      it.remove();
      busyHosts.put(freeHost, newTaskTime);
    }
    return freeHost;
  }

  /**
   * Marks given <code>host</code> as <i>busy</i>.
   * 
   * @param host
   * @param lassAccessTime
   * @throws NullArgumentException if <code>host</code> is <code>null</code>.
   */
  public void addAccessedHost(final String host, long lassAccessTime)
  {
    if (host == null)
      throw new NullArgumentException("host");

    // Long time = (Long) busyHosts.get(host);
    // if (time == null) {
    // logger.error("Cannot complete task that is not busy: host=" + host);
    // throw new IllegalStateException("Cannot complete task that is not busy: host=" + host);
    // }
    busyHosts.put(host, lassAccessTime);
  }

  /**
   * Returns number of free hosts, i.e. hosts ready to be accessed now.
   * 
   * @return number of free hosts.
   */
  public int getFreeHostsNum()
  {
    return freeHosts.size();
  }

  /**
   * Returns number of busy hosts, i.e. hosts that are not ready to be accessed now.
   * 
   * @return number of busy hosts.
   */
  public int getBusyHostsNum()
  {
    return busyHosts.size();
  }

  // /**
  // * Returns
  // *
  // * @return
  // */
  // public long getServerDelay()
  // {
  // return SERVER_DELAY;
  // }

  /**
   * Updates host access status.<br>
   * 
   * <pre>
   *     For every host marked as busy updating process will:
   *      + unmark it if elapsed time is more then SERVER_DELAY and 
   *      + mark it as free if its elapsed after last access is more 
   *        then SERVER_DELAY and if there is at least one task in
   *        hostTaskDB database.
   * </pre>
   * 
   * @return minimum time in millis. before some host will be <i>free</i>.
   */
  public long update()
  {
    long time = 0;
    Iterator<Map.Entry<String, Long>> it = busyHosts.entrySet().iterator();
    while (it.hasNext()) {
      Map.Entry<String, Long> entry = (Map.Entry<String, Long>) it.next();
      long lastAccessTime = entry.getValue();
      if (lastAccessTime == 0)
        continue;
      long elapsedTime = System.currentTimeMillis() - lastAccessTime;
      if (elapsedTime > SERVER_DELAY) {
        it.remove();

        Object task = hostTaskDB.get(entry.getKey());
        if (task != null) {
          if (logger.isDebugEnabled())
            logger.debug("Host from busy is added to free: host=" + entry.getKey());
          freeHost(entry.getKey());
        } else {
          time = elapsedTime - SERVER_DELAY; // todo: does it make sense?
        }
      } else {
        time = SERVER_DELAY - elapsedTime;
      }
    }
    return time;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("freeHosts.size",
                                                                              freeHosts.size())
        .append("busyHosts.size", busyHosts).toString();
  }

}

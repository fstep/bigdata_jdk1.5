/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler;

import com.porva.crawler.thread.WorkersFixedThreadPool;

/**
 * Crawler's control interface.
 * 
 * @author Poroshin V.
 * @date Sep 14, 2005
 */
public interface Crawler extends Stat // , WorkerService
{

  /**
   * Status of crawler.
   * <p>
   * Crawler always has some status according to a diagram:<br>
   * <img src="../../../pic/CrawlerStatus-diagram.gif">
   */
  public enum CrawlerStatus
  {
    /**
     * Crawler is running.
     */
    RUNNING,

    /**
     * Crawler is resuming after pausing.
     */
    RESUMING,

    /**
     * Crawler has finished its work. All tasks from worload were processed.
     */
    DONE,

    /**
     * Crawler is ready to start.
     */
    READY,

    /**
     * Crawler is not constructed or not ready to start.
     */
    UNDEF,

    /**
     * Crawler is in process of initializing. Propably it is adding initial tasks.
     */
    INITIALIZING,

    /**
     * Crawler is in process of suspending.
     * 
     * @see #SUSPENDED as a next stage of crawler.
     */
    SUSPENDING,

    /**
     * Crawler is suspended. It this stage crawler finished its work as
     * <code>CrawlerStatus.DONE</code> but workload is not empty.
     */
    SUSPENDED,

    /**
     * Crawler is in process of pausing.
     * 
     * @see #PAUSED as a next stage of crawler.
     */
    PAUSING,

    /**
     * Crawler is paused and can be resumed.
     */
    PAUSED,
  }

  // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // crawler flow control functions

  /**
   * Start crawler.
   */
  public void startWork();

  /**
   * Schedule to pause crawler work.
   */
  public void pauseWork();

  /**
   * Resume crawler work.
   */
  public void resumeWork();

  /**
   * Schedule to suspend crawler work.
   */
  public void suspendWork();

  /**
   * Force to interrupt all work.
   */
  public void interruptWork();

  /**
   * Blocks current thread until crawler will change its status to specified <code>status</code>.
   * 
   * @param status to wailt.
   * @throws InterruptedException
   * @throws org.apache.commons.lang.NullArgumentException if <code>status</code> is
   *           <code>null</code>.
   */
  public void waitUntil(CrawlerStatus status) throws InterruptedException;

  // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // crawler status functions

  /**
   * Returns crawler status.
   * 
   * @return crawler status.
   */
  public CrawlerStatus getStatus();

  /**
   * This function called by {@link WorkersFixedThreadPool} object to indicate that its state has
   * been changed.
   * 
   * @param newState new {@link WorkersFixedThreadPool.State} state of workers pool.
   * @throws org.apache.commons.lang.NullArgumentException if <code>newState</code> is
   *           <code>null</code>.
   */
  public void workersPoolStateChanged(final WorkersFixedThreadPool.State newState);

}

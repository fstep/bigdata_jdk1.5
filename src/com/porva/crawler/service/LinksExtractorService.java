/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.service;

import java.util.List;

import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.task.FetchResult;

/**
 * Service that extracts and resolves links.
 * 
 * @author Poroshin V.
 * @date Oct 14, 2005
 */
public interface LinksExtractorService extends WorkerService
{
  /**
   * Extracts links from content body of given <code>fetchTaskResult</code>.
   * 
   * @param fetchTaskResult
   * @param listBuff list to store extracted links.
   * @return number of extracted links.
   * @throws org.apache.commons.lang.NullArgumentException if <code>fetchTaskResult</code> or
   *           <code>listBuff</code> is <code>null</code>.
   */
  public int extractLinks(FetchResult fetchTaskResult, List<CrawlerURL> listBuff);

  /**
   * Extracts links from given <code>content</code> and resolves them against
   * <code>crawlerURL</code>.
   * 
   * @param crawlerURL url of this content.
   * @param content content to extract links from.
   * @param listBuff list to store extracted links.
   * @return number of extracted links.
   * @throws org.apache.commons.lang.NullArgumentException if <code>crawlerURL</code> or
   *           <code>content</code> or <code>listBuff</code> is <code>null</code>.
   */
  public int extractLinks(CrawlerURL crawlerURL, byte[] content, List<CrawlerURL> listBuff);

  public int resolveLinks(CrawlerURL baseURL, List<String> links, List<CrawlerURL> listBuff);
}

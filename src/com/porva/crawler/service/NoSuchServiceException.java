/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.service;

/**
 * Excetion that signals that requested service is not available.
 * 
 * @author Poroshin V.
 * @date Sep 15, 2005
 */
public class NoSuchServiceException extends ServiceException
{
  private static final long serialVersionUID = 1372279453505891170L;
  
  /**
   * Constructs a <code>NoSuchServiceException</code> with the specified
   * detail message. The error message string <code>msg</code> can later be
   * retrieved by the <code>{@link java.lang.Throwable#getMessage}</code>
   * method of class <code>java.lang.Throwable</code>.
   * 
   * @param msg the detail message.
   */
  public NoSuchServiceException(String msg)
  {
    super(msg);
  }
}

/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.service;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * This class provides default implementaion of
 * {@link com.porva.crawler.service.WorkerService} interface. <br>
 * 
 * @author Poroshin V.
 * @date Sep 15, 2005
 */
public abstract class AbstractWorkerService extends AbstractService implements WorkerService
{

  /**
   * Allocates new {@link AbstractWorkerService} object with specified service name.
   * 
   * @param name name of service.
   * @throws NullArgumentException if <code>name</code> is <code>null</code>.
   */
  public AbstractWorkerService(String name)
  {
    super(name);
  }

  /**
   * Marks service as stopped.<br>
   * Method <code>isStopped()</code> of stopped service will return
   * <code>true</code>. If service is already stopped then warning to logger
   * will be written.<br>
   * <br>
   * Consider to overwrite this method in child classes to perform additional
   * finalization operations. Call <code>super.stop()</code> in your child
   * overwritten <code>stop()</code> method to mark service as stopped.
   */
  public void stop() throws ServiceException
  {
    super.stop();
  }

  private transient String toString;

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    if (toString == null) {
      toString = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
          .appendSuper(super.toString()).toString();
    }
    return toString;
  }
  
  
}

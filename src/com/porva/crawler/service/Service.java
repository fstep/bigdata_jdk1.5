/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

package com.porva.crawler.service;

/**
 * This interface defines protocol for any classes which can be used as service.<br>
 * Throw {!@link com.porva.crawler.service.ServiceProvider} object any service can be requested and
 * casted to actual service object.
 * 
 * @author Poroshin V.
 * @date Sep 15, 2005
 */
public interface Service
{

//  /**
//   * Initialize service with parameter string.
//   * 
//   * @param params parameters.
//   * @throws IllegalArgumentException if <code>params</code> string is invalid.
//   */
//  public void init(String params);

  /**
   * Returns name of service.
   * 
   * @return name of service.
   */
  public String getServiceName();

  /**
   * Stops the service.<br>
   * After this you should not use the service.
   * 
   * @throws ServiceException if some failure occurs.
   */
  public void stop() throws ServiceException;

  /**
   * Returns <code>true</code> if this service is stopped.
   * 
   * @return <code>true</code> if this service is stopped; <code>false</code> otherwise.
   */
  public boolean isStopped();
}

/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.manager.Manager;
import com.porva.crawler.service.NoSuchServiceException;
import com.porva.crawler.service.Service;
import com.porva.crawler.service.ServiceException;
import com.porva.crawler.service.ServiceProvider;
import com.porva.crawler.task.EmptyTask;
import com.porva.crawler.task.LastTask;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskFormatException;
import com.porva.crawler.thread.WorkersFixedThreadPool;
import com.porva.crawler.workload.Workload;

/**
 * @author Poroshin V.
 * @date Sep 14, 2005
 */
public class DefaultCrawler extends Thread implements Crawler
{
  private CrawlerStatus status = CrawlerStatus.UNDEF;

  private Frontier frontier;

  private Manager manager;

  private ServiceProvider services;

  private WorkersFixedThreadPool workersPool;

  private int updateTime;

  private long startTime;

  private static Log logger = LogFactory.getLog(DefaultCrawler.class);

  /**
   * Constructs a new crawler object.
   * 
   * @param updateTime
   * @param services
   * @param workersNum
   * @throws ServiceException
   * @throws NullArgumentException
   * @throws IllegalArgumentException
   */
  public DefaultCrawler(final int updateTime, final ServiceProvider services, final int workersNum)
      throws ServiceException
  {
    this.services = services;
    this.updateTime = updateTime;
    if (this.services == null)
      throw new NullArgumentException("servises");
    if (workersNum <= 0)
      throw new IllegalArgumentException("workersNum must be > 0: " + workersNum);
    if (this.updateTime < 0)
      throw new IllegalArgumentException("updateTime must be >= 0:" + updateTime);

    try {
      this.frontier = (Frontier) services.getService("commonservice.frontier");
      this.manager = (Manager) services.getService("commonservice.manager");
    } catch (NoSuchServiceException e) {
      logger.fatal(e);
      throw new IllegalArgumentException(e);
    }

    workersPool = new WorkersFixedThreadPool(workersNum, this, frontier, this.services);
    setStatus(CrawlerStatus.READY);
  }

  public void run()
  {
    logger.info("Added " + addInitialTasks() + " initial tasks");
    logger.info("Crawler has started its main loop.");

    startTime = System.currentTimeMillis();

    while (status != CrawlerStatus.DONE) {
      try {
        process();
        sleep(updateTime);
      } catch (InterruptedException e) {
        logger.warn("Sleeping in crawler's loop is interrupted: ", e);
      }
    }

    if (logger.isInfoEnabled())
      logger.info("Crawler has finished its main loop.");
  }

  private int addInitialTasks()
  {
    int addedTasksNum = 0;

    // initialize before main loop
    Task initialTask = null; // FIXME if first task is invalid then no tasks will be added!
    while (true) {
      try {
        initialTask = manager.getNextInitialTask();
        if (initialTask == null)
          break;
        frontier.addTask(initialTask);
        addedTasksNum++;
        if (addedTasksNum % 100 == 0)
          logger.info("Added initial tasks to workload: " + addedTasksNum);
      } catch (IOException e) {
        logger.fatal("Cannot read initial tasks because of I/O fatal problems", e);
        break;
      } catch (TaskFormatException e) {
        logger.warn("Invalid initial task: " + initialTask);
      }
    }
    return addedTasksNum;
  }

  private void process() throws InterruptedException
  {
    Task<?> task = frontier.getTask();
    if (task instanceof LastTask) {
      workersPool.stop();
      workersPool.waitUntil(WorkersFixedThreadPool.State.DONE); // todo: what happened if it will be
      // interrupted ???
      return;
    }

    if (task instanceof EmptyTask) {
      logger.debug("Waiting for new tasks from workload...");
      return;
    }

    workersPool.assign(task);
  }

  public synchronized void startWork()
  {
    this.start();
  }

  public synchronized void pauseWork()
  {
    setStatus(CrawlerStatus.PAUSING);
    workersPool.pause();
  }

  public synchronized void resumeWork()
  {
    setStatus(CrawlerStatus.RESUMING);
    workersPool.resume();
  }

  public synchronized void suspendWork()
  {
    setStatus(CrawlerStatus.SUSPENDING);
    workersPool.stop();
  }

  public synchronized void interruptWork()
  {
    // TODO Auto-generated method stub

  }

  public synchronized void waitUntil(final CrawlerStatus status) throws InterruptedException
  {
    while (this.status != status)
      wait();
  }

  /**
   * Returns {@link CrawlerStatus} status of the crawler.
   * 
   * @return status of the crawler.
   */
  public synchronized CrawlerStatus getStatus()
  {
    return status;
  }

  public synchronized void workersPoolStateChanged(final WorkersFixedThreadPool.State newState)
  {
    if (newState == null)
      throw new NullArgumentException("newState");

    switch (newState) {
    case SLEEPING:
      // todo: case SLEEPING
      break;
    case PAUSED:
      setStatus(CrawlerStatus.PAUSED);
      break;
    case RUNNING:
      if (status != CrawlerStatus.RUNNING)
        setStatus(CrawlerStatus.RUNNING);
      break;
    case DONE:
      if (status == CrawlerStatus.SUSPENDING)
        setStatus(CrawlerStatus.SUSPENDED);
      else
        setStatus(CrawlerStatus.DONE);
      break;
    default:
      break;
    }
  }

  public synchronized Map<String, String> getStat()
  {
    final Map<String, String> stat = new HashMap<String, String>();
    stat.put("crawler.time-started", Long.toString(startTime));
    stat.put("crawler.time-loopdelay", Integer.toString(updateTime));
    stat.put("crawler.status", getStatus().toString());
    for (Map.Entry<String, Service> entry : services.getAllServices().entrySet()) {
      stat.put("crawler." + entry.getKey(), entry.getValue().toString());
    }

    stat.put("crawler.workersPool.state", workersPool.getPoolState().toString());

    try {
      stat.putAll(((Workload) services.getService("commonservice.workload")).getStat());
    } catch (Exception e) {
      logger.warn("Failed to get statistics from workload", e);
      e.printStackTrace();
    }

    stat.putAll(manager.getStat());
    stat.put("crawler.toString", toString()); 

    return stat;
  }

  // /////////////////////////////////////////////////////////
  // private fucntions
  // /////////////////////////////////////////////////////////

  private synchronized void setStatus(final CrawlerStatus newStatus)
  {
    assert newStatus != null;

    this.status = newStatus;
    manager.crawlerStatusChanged(newStatus);
    logger.info("Crawler status changed: " + this.status);

    notifyAll();
  }

  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("status", status)
        .append("frontier", frontier).append("manager", manager).append("services", services)
        .append("workersPool", workersPool).append("updateTime", updateTime).toString();
  }

}

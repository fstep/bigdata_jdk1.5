/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.task;

import static com.porva.crawler.task.FetchResult.Status.COMPLETE;
import static com.porva.crawler.task.FetchResult.Status.HTTP_CLIENT_EXCEPTION;
import static com.porva.crawler.task.FetchResult.Status.TASK_FILTERED;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.Frontier;
import com.porva.crawler.db.CrawlerDBValueFactory;
import com.porva.crawler.db.DBException;
import com.porva.crawler.db.DBInputStream;
import com.porva.crawler.db.DBOutputStream;
import com.porva.crawler.db.DBValue;
import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.net.HttpClientException;
import com.porva.crawler.net.MalformedCrawlerURLException;
import com.porva.crawler.net.Response;
import com.porva.crawler.service.NoSuchServiceException;
import com.porva.crawler.service.ServiceProvider;

import de.nava.informa.core.ChannelIF;
import de.nava.informa.core.ItemIF;
import de.nava.informa.core.ParseException;
import de.nava.informa.impl.basic.ChannelBuilder;
import de.nava.informa.parsers.FeedParser;

public class FetchRSSTask extends DefaultFetchTask
{
  private static Log logger = LogFactory.getLog(FetchRSSTask.class);

  public FetchResult perform(ServiceProvider serviceProvider)
  {
    if (serviceProvider == null)
      throw new NullArgumentException("serviceProvider");

    FetchRSSResult taskResult = null;
    try {
      if (!isValidFetchTask(serviceProvider))
        return new FetchRSSResult(this, TASK_FILTERED, null);

      Response response = fetchURL(serviceProvider);
      taskResult = new FetchRSSResult(this, COMPLETE, response);

      if (isRedirect(response))
        processRedirectResponse(serviceProvider, response);

      if (isValidFetchTaskToProduce(serviceProvider)) {
        if (isOk(response))
          processOkResponse(serviceProvider, taskResult);
      }

    } catch (NoSuchServiceException e) {
      throw new UnhandledException(e);
    } catch (HttpClientException e) {
      taskResult = new FetchRSSResult(this, HTTP_CLIENT_EXCEPTION, null);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (ParseException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return taskResult;
  }

  void processOkResponse(final ServiceProvider serviceProvider, final FetchRSSResult result)
      throws NoSuchServiceException, IOException, ParseException
  {
    // List<FetchTask> fetchTasks
    byte[] content = result.getResponse().getBody();
    ChannelIF channel = FeedParser.parse(new ChannelBuilder(), new ByteArrayInputStream(content));

    List<ItemIF> items = new ArrayList<ItemIF>();
    for (Object item : channel.getItems()) {
      items.add((ItemIF) item);
    }
    if (logger.isInfoEnabled())
      logger.info("Readed " + items.size() + " RRS articles");

    List<CrawlerURL> urls = extractLinks(items, result.getTask().getCrawlerURL());
    List<Task> tasks = urlsToTasks(urls);

    addTasksToCrawler((Frontier) serviceProvider.getService("commonservice.frontier"), tasks);
  }

  List<Task> urlsToTasks(List<CrawlerURL> urls)
  {
    List<Task> tasks = new ArrayList<Task>();
    for (CrawlerURL url : urls)
      tasks.add(TaskFactory.newFetchURL(url));

    return tasks;
  }

  void addTasksToCrawler(final Frontier frontier, final List<Task> tasks)
  {
    int addedTasksNum = frontier.addTaskList(tasks);
    if (logger.isInfoEnabled())
      logger.info("Added " + addedTasksNum + " tasks to workload.");
  }

  private List<CrawlerURL> extractLinks(List<ItemIF> allRssFeeds, CrawlerURL parentTask)
  {
    List<CrawlerURL> links = new ArrayList<CrawlerURL>();
    for (ItemIF item : allRssFeeds) {
      URL link = item.getLink();
      if (link == null) {
        logger.warn("RSS item does not have link to original article!");
      } else {
        try {
          CrawlerURL crawlerURI = new CrawlerURL(parentTask.getDepth() + 1, link.toURI().toString(),
              parentTask.getRedirNum());
          links.add(crawlerURI);
        } catch (URISyntaxException e) {
          logger.warn("Failed to create CrawlerURI for rss link: " + link);
        } catch (MalformedCrawlerURLException e) {
          logger.warn("Failed to create CrawlerURI for rss link: " + link);
        }
      }
    }
    return links;
  }

  void processRedirectResponse(ServiceProvider serviceProvider, Response response)
      throws NoSuchServiceException
  {
    CrawlerURL redirectURL = getRedirectURL(response);
    if (redirectURL != null) {
      FetchTask redirectTask = (FetchTask) TaskFactory.newFetchURL(redirectURL);
      ((Frontier) serviceProvider.getService("commonservice.frontier")).addTask(redirectTask);
    }
  }

  protected FetchRSSTask()
  {
    super(defCrawlerURL);
  }

  FetchRSSTask(final CrawlerURL crawlerURL)
  {
    super(crawlerURL);
  }

  public void writeTo(DBOutputStream tupleOutput)
  {
    if (tupleOutput == null)
      throw new NullArgumentException("tupleOutput");

    getCrawlerURL().writeTo(tupleOutput);
  }

  public DBValue newInstance(DBInputStream tupleInput) throws DBException
  {
    if (tupleInput == null)
      throw new NullArgumentException("tupleInput");

    CrawlerURL crawlerURL = null;
    try {
      crawlerURL = (CrawlerURL) CrawlerDBValueFactory.instance.newDBValueInstance(tupleInput,
                                                                                  CrawlerURL.class);
    } catch (Exception e) {
      throw new DBException(e);
    }

    return new FetchRSSTask(crawlerURL);
  }

  public long getClassCode()
  {
    return 7234124506111L;
  }

  public boolean equals(final Object other)
  {
    return new EqualsBuilder().appendSuper(super.equals(other)).isEquals();
  }

  private transient int hashCode;

  public int hashCode()
  {
    if (hashCode == 0) {
      hashCode = new HashCodeBuilder().appendSuper(super.hashCode()).toHashCode();
    }
    return hashCode;
  }
}

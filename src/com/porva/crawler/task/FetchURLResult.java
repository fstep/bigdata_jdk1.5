/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.task;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.porva.crawler.db.CrawlerDBValueFactory;
import com.porva.crawler.db.DBException;
import com.porva.crawler.db.DBInputStream;
import com.porva.crawler.db.DBOutputStream;
import com.porva.crawler.db.DBValue;
import com.porva.crawler.net.Response;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;

public class FetchURLResult implements FetchResult
{
  private DefaultFetchResult defFetchResult;

  private String charset;

  private String parsed;

  private String title;

  private FetchURLResult()
  {
  }

  public FetchURLResult(final FetchTask fetchTask, final Status status, final Response response)
  {
    defFetchResult = new DefaultFetchResult(fetchTask, status, response);
  }

  private FetchURLResult(DefaultFetchResult defFetchResult)
  {
    this.defFetchResult = defFetchResult;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DBValue#getClassCode()
   */
  public long getClassCode()
  {
    return -8913534021L;
  }

  public Status getStatus()
  {
    return defFetchResult.getStatus();
  }

  public Response getResponse()
  {
    return defFetchResult.getResponse();
  }

  public FetchTask getTask()
  {
    return defFetchResult.getTask();
  }

  public String getParsed()
  {
    return parsed;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }

  public void setParsed(final String parsed)
  {
    this.parsed = parsed;
  }

  public void writeTo(final DBOutputStream tupleOutput)
  {
    if (tupleOutput == null)
      throw new NullArgumentException("tupleOutput");

    tupleOutput.writeLong(defFetchResult.getClassCode());
    defFetchResult.writeTo(tupleOutput);

    tupleOutput.writeString(charset);
    tupleOutput.writeString(parsed);
    tupleOutput.writeString(title);
  }

  public DBValue newInstance(final DBInputStream tupleInput) throws DBException
  {
    if (tupleInput == null)
      throw new NullArgumentException("tupleInput");

    DefaultFetchResult dfr = (DefaultFetchResult) CrawlerDBValueFactory.instance
        .readObject(tupleInput);

    FetchURLResult res = new FetchURLResult(dfr);
    res.setCharset(tupleInput.readString());
    res.setParsed(tupleInput.readString());
    res.setTitle(tupleInput.readString());
    return res;
  }

  public String getDBKey()
  {
    return defFetchResult.getDBKey();
  }

  public DBValue getDBValue()
  {
    return this;
  }

  public String getCharset()
  {
    return charset;
  }

  public void setCharset(final String charset)
  {
    this.charset = charset;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("defFetchResult",
                                                                              defFetchResult)
        .append("charset", charset).append("title", title).append("parsed", parsed).toString();
  }

  public boolean equals(final Object other)
  {
    if (!(other instanceof FetchURLResult))
      return false;
    FetchURLResult castOther = (FetchURLResult) other;
    return new EqualsBuilder().append(defFetchResult, castOther.defFetchResult).isEquals();
  }

  public int hashCode()
  {
    return new HashCodeBuilder().append(defFetchResult).toHashCode();
  }

}

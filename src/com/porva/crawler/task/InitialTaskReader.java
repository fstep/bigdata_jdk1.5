/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.task;

import java.io.IOException;

/**
 * Interface for initial tasks readers.
 * 
 * @author Poroshin V.
 * @date Sep 22, 2005
 */
public interface InitialTaskReader
{
  /**
   * Returns next initial task or <code>null</code> if no more initial tasks available.
   * 
   * @return next initial task or <code>null</code> if no more initial tasks available.
   * @throws IOException if some io exception occurs during reading of next task.
   * @throws TaskFormatException if next task cannot be readed because it is in incorrect format.
   */
  public Task getNextTask() throws IOException, TaskFormatException;

  /**
   * Closes this reader.
   * 
   * @throws IOException
   */
  public void close() throws IOException;

  /**
   * Returns <code>true</code> if this reader is closed; <code>false</code> otherwise. 
   * 
   * @return <code>true</code> if this reader is closed; <code>false</code> otherwise.
   */
  public boolean isClosed();
}

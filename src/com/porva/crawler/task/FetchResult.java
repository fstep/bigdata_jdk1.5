/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.task;

import com.porva.crawler.db.DBRecord;
import com.porva.crawler.net.Response;

/**
 * Defined a {@link TaskResult} of {@link FetchTask}.
 * 
 * @author Poroshin V.
 * @date Oct 15, 2005
 */
public interface FetchResult extends TaskResult<FetchTask>, DBRecord
{
  
  /**
   * Status of fetch result.<br>
   * 
   * @author Poroshin V.
   * @date Oct 7, 2005
   */
  public static enum Status
  {
    /**
     * Denotes complete fetch task result, i.e. task with not null Response.
     */
    COMPLETE((byte) 0),

    /**
     * Denotes failed fetch task result because creation task was filtered out.
     */
    TASK_FILTERED((byte) 1),

    /**
     * Denotes failed fetch task result because some http client exception occurs.
     */
    HTTP_CLIENT_EXCEPTION((byte) 2);

    private final byte byteVal;

    private Status(byte byteVal)
    {
      this.byteVal = byteVal;
    }

    /**
     * Returns single byte representation of this status.
     * 
     * @return single byte representation of this status.
     */
    public byte asByte()
    {
      return byteVal;
    }

    /**
     * Returns {@link Status} instance that has <code>byteVal</code> byte representation.
     * 
     * @param byteVal
     * @return {@link Status} instance that has <code>byteVal</code> byte representation.
     * @throws IllegalArgumentException if <code>byteVal</code> does not represent any status.
     */
    public static Status getInstance(byte byteVal)
    {
      switch (byteVal) {
      case 0:
        return COMPLETE;
      case 1:
        return TASK_FILTERED;
      case 2:
        return HTTP_CLIENT_EXCEPTION;
      }
      throw new IllegalArgumentException("This byte value does not represent any status: "
          + byteVal);
    }

  }

  
  /**
   * Returns status of fetch result.
   * 
   * @return status of fetch result.
   */
  public Status getStatus();
  
  /**
   * Returns response of the fetch reasult or <code>null</code> if result is failed.
   * 
   * @return response of the fetch reasult or <code>null</code> if result is failed.
   */
  public Response getResponse();

}

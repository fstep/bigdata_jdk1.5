/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.task;

import static com.porva.crawler.task.FetchResult.Status.COMPLETE;
import static com.porva.crawler.task.FetchResult.Status.HTTP_CLIENT_EXCEPTION;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.porva.crawler.db.DBException;
import com.porva.crawler.db.DBInputStream;
import com.porva.crawler.db.DBOutputStream;
import com.porva.crawler.db.DBValue;
import com.porva.crawler.db.CrawlerDBValueFactory;
import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.net.HttpClientException;
import com.porva.crawler.net.Response;
import com.porva.crawler.service.NoSuchServiceException;
import com.porva.crawler.service.ServiceProvider;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;

class FetchRobotxt extends DefaultFetchTask
{

  private int redirectsLimit = 3;

  protected FetchRobotxt()
  {
    super(defCrawlerURL);
  }

  FetchRobotxt(CrawlerURL crawlerURL)
  {
    super(crawlerURL);
  }

  public FetchResult perform(ServiceProvider serviceProvider)
  {
    if (serviceProvider == null)
      throw new NullArgumentException("serviceProvider");

    FetchRobotxtResult taskResult = null;
    try {
      Response response = fetchURL(serviceProvider);
      taskResult = new FetchRobotxtResult(this, COMPLETE, response);

      if (isOk(response)) {
        return taskResult;
      }

      if (isRedirect(response)) {
        // check if redir limit is reached
        if (getCrawlerURL().getRedirNum() > redirectsLimit)
          return taskResult;

        // get redirect location
        CrawlerURL redirectURL = getRedirectURL(response);
        if (redirectURL == null)
          return taskResult;

        // check if redir location ends with robots.txt
        if (!redirectURL.getURLString().endsWith("robots.txt"))
          return taskResult;

        // construct new FetchRobotxt task and perform it
        Task task = TaskFactory.newFetchRobotxt(redirectURL);
        FetchRobotxtResult result = (FetchRobotxtResult) task.perform(serviceProvider);
        taskResult.setRedirectResult(result);
      }

    } catch (NoSuchServiceException e) {
      throw new UnhandledException(e);
    } catch (HttpClientException e) {
      taskResult = new FetchRobotxtResult(this, HTTP_CLIENT_EXCEPTION, null);
    }
    return taskResult;
  }

  public void writeTo(final DBOutputStream tupleOutput)
  {
    if (tupleOutput == null)
      throw new NullArgumentException("tupleOutput");

    getCrawlerURL().writeTo(tupleOutput);
  }

  public DBValue newInstance(final DBInputStream tupleInput) throws DBException
  {
    if (tupleInput == null)
      throw new NullArgumentException("tupleInput");

    CrawlerURL crawlerURL = null;
    try {
      crawlerURL = (CrawlerURL) CrawlerDBValueFactory.instance.newDBValueInstance(tupleInput,
                                                                                  CrawlerURL.class);
    } catch (Exception e) {
      throw new DBException(e);
    }
    return new FetchRobotxt(crawlerURL);
  }

  public long getClassCode()
  {
    return 7036412903421L;
  }

  private transient String toString;

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    if (toString == null) {
      toString = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
          .appendSuper(super.toString()).toString();
    }
    return toString;
  }

  public boolean equals(final Object other)
  {
    return new EqualsBuilder().appendSuper(super.equals(other)).isEquals();
  }

  private transient int hashCode;

  public int hashCode()
  {
    if (hashCode == 0) {
      hashCode = new HashCodeBuilder().appendSuper(super.hashCode()).toHashCode();
    }
    return hashCode;
  }
}

/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.task;

import com.porva.crawler.net.CrawlerURL;

/**
 * Tasks factory class. 
 * 
 * @author Poroshin V.
 * @date Oct 10, 2005
 */
public class TaskFactory
{
//  /**
//   * Empty task. Denotes that no tasks are yet available. Causes worker to wait for a new task.
//   */
//  EMPTY((byte) 1, Long.MAX_VALUE),

//  /**
//   * Denotes that whit is the last task in wokload and workers should exit.
//   */
//  LAST((byte) 2, Long.MAX_VALUE),

//  /**
//   * Download CrawlerURL task.
//   */
//  FETCH_URL((byte) 3, CrawlerSettings.FETCH_TASK_LIFETIME),

//  /**
//   * Download robots.txt file.
//   */
//  FETCH_ROBOTSTXT((byte) 4, CrawlerSettings.ROBOTXT_LIFETIME);
  
  // FETCH_RSS((byte) 5),

//  private byte byteVal;
//
//  private long lifetime;
//
//  TaskFactory(byte byteVal, long lifetime)
//  {
//    this.byteVal = byteVal;
//    this.lifetime = lifetime;
//  }
//
//  /**
//   * Returns a view of this task type as a byte.
//   * 
//   * @return view of this task type as a byte.
//   */
//  public byte asByte()
//  {
//    return byteVal;
//  }
  
  public static Task newFetchURL(final CrawlerURL crawlerURL)
  {
    return new FetchURL(crawlerURL);
  }
  
  public static Task newFetchRobotxt(final CrawlerURL crawlerURL)
  {
    return new FetchRobotxt(crawlerURL);
  }
  
  public static Task newEmptyTask()
  {
    return EmptyTask.getInstance();
  }
  
  public static Task newLastTask()
  {
    return LastTask.getInstance();
  }
  
  public static Task newFetchRSS(final CrawlerURL crawlerURL)
  {
    return new FetchRSSTask(crawlerURL);
  }
  
  public static Task newGoogleTask(int num, String query, int depth)
  {
    return new GoogleTask(num, query, depth);
  }
  
//  public static Task newRedirectTaskWrapper(final FetchTask taskToWrap)
//  {
//    return new RedirectTaskWrapper(taskToWrap);
//  }


//  /**
//   * Returns instance of {@link TaskFactory} with specified <code>byteVal</code> view.
//   * 
//   * @param byteVal one byte view of requested instance of {@link TaskFactory}.
//   * @return instance of {@link TaskFactory} with specified <code>byteVal</code> view.
//   * @throws IllegalArgumentException if <code>byteVal</code> is not a view of any
//   *           {@link TaskFactory} intances.
//   */
//  public static TaskFactory getInstance(byte byteVal)
//  {
//    switch (byteVal) {
////    case 1:
////      return EMPTY;
////    case 2:
////      return LAST;
////    case 3:
////      return FETCH_URL;
//    case 4:
//      return FETCH_ROBOTSTXT;
//    }
//
//    throw new IllegalArgumentException("No such instances with byte value: " + byteVal);
//  }

//  /**
//   * Returns task lifetime in milliseconds. After this lifetime expires the task of this type should
//   * be updated.
//   * 
//   * @return lifetime of this type of task in milliseconds.
//   */
//  public long getTaskLifetime()
//  {
//    return lifetime;
//  }

//  /**
//   * Constructs a new Task object of this type with specified input parameters <code>params</code>.<br>
//   * Be sure to pass valid <code>params</code> objects.
//   * 
//   * @param params parametes for constructing new task of this type, or <code>null</code> if no
//   *          parameters required.
//   * @return new {@link Task} instance of this task type.
//   * @throws IllegalClassException if some parameter cannot be casted to required type.
//   * @throws NotImplementedException if this task type cannot create its task instances.
//   */
//  public Task<?> newTask(final Object[] params)
//  {
//    switch (byteVal) {
////    case 1:
////      return EmptyTask.getInstance();
////    case 2:
////      return LastTask.getInstance();
//    case 3:
//      if (params[0] instanceof CrawlerURL == false)
//        throw new IllegalClassException(CrawlerURL.class, params[0]);
//      return new FetchURL((CrawlerURL) params[0]);
//    case 4: // todo: create special FetchRobotxtTask
//      if (params[0] instanceof CrawlerURL == false)
//        throw new IllegalClassException(CrawlerURL.class, params[0]);
//      return new FetchURL((CrawlerURL) params[0]);
//    }
//    throw new NotImplementedException("This task type cannot create new Tasks instances: "
//        + this.toString());
//  }

//  /**
//   * Constructs a new Task object of this type with specified input parameter <code>param</code>.<br>
//   * Be sure to pass valid <code>param</code> object.
//   * 
//   * @param param paramete for constructing new task of this type, or <code>null</code> if no
//   *          parameters required.
//   * @return new {@link Task} instance of this task type.
//   * @throws IllegalClassException if <code>param</code> cannot be casted to required type.
//   * @throws NotImplementedException if this task type cannot create its task instances.
//   */
//  public Task newTask(final Object param)
//  {
//    switch (byteVal) {
////    case 1:
////      return EmptyTask.getInstance();
////    case 2:
////      return LastTask.getInstance();
//    case 3:
//      if (param instanceof CrawlerURL == false)
//        throw new IllegalClassException(CrawlerURL.class, param);
//      return new FetchURL((CrawlerURL) param);
//    case 4:
//      if (param instanceof CrawlerURL == false)
//        throw new IllegalClassException(CrawlerURL.class, param);
//      return new FetchURL((CrawlerURL) param);
//    }
//    throw new NotImplementedException("This task type cannot create new Tasks instances: "
//        + this.toString());
//  }

//  /**
//   * 
//   * @param taskResult
//   * @return new update task
//   */
//  public Task newUpdateTask(final TaskResult taskResult)
//  {
//    switch (byteVal) {
//    case 1:
//      return EmptyTask.getInstance();
//    case 2:
//      return LastTask.getInstance();
//    case 3:
//      if (taskResult == null)
//        throw new NullArgumentException("taskResult");
//      return createFetchURLUpdateTask((DefaultFetchResult) taskResult);
//    // case 4: // todo: ???
//    // return new FetchURL((CrawlerURL) param);
//    }
//    throw new NotImplementedException("This task type cannot create new Tasks instances: "
//        + this.toString());
//  }

//  private Task createFetchURLUpdateTask(final DefaultFetchResult result)
//  {
//    assert result != null;
//    
//    CrawlerURL crawlerURL = new CrawlerURL(result.getTask().getCrawlerURL());
//    return FETCH_URL.newTask(crawlerURL);
//  }

}

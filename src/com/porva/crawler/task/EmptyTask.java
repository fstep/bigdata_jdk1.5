/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.task;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.porva.crawler.db.*;
import com.porva.crawler.service.ServiceProvider;

/**
 * Denotes an empty task, i.e. task cannot be perfomed that should be ignored.<br>
 * 
 * @author Poroshin V.
 * @date Oct 13, 2005
 */
public class EmptyTask implements Task
{

  private static final EmptyTask instance = new EmptyTask();
  
  static EmptyTask getInstance()
  {
    return instance;
  }

  private EmptyTask()
  {
  }

  public TaskResult perform(ServiceProvider serviceProvider)
  {
    throw new IllegalStateException();
  }

  public void writeTo(DBOutputStream tupleOutput)
  {
  }

  public DBValue newInstance(DBInputStream tupleInput)
  {
    return instance;
  }

  public long getClassCode()
  {
    return 740098564027907L;
  }

  private transient String toString;

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    if (toString == null) {
      toString = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
          .append("classCode", getClassCode()).toString();
    }
    return toString;
  }

  public String getDBKey()
  {
    return "empty-task";
  }

  public DBValue getDBValue()
  {
    return instance;
  }

}

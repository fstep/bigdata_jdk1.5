/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.net;

/**
 * Thrown to indicate that a malformed CrawlerURL has occurred.
 * 
 * @author Poroshin V.
 * @date Sep 16, 2005
 */
public class MalformedCrawlerURLException extends Exception
{

  private static final long serialVersionUID = 8817883754303468146L;

  /**
   * Constructs an <code>MalformedCrawlerURLException</code> with <code>null</code>
   * as its error detail message.
   */
  public MalformedCrawlerURLException()
  {
    super();
  }

  /**
   * Constructs a <code>MalformedCrawlerURLException</code> with the specified detail
   * message. The error message string <code>s</code> can later be
   * retrieved by the <code>{@link java.lang.Throwable#getMessage}</code>
   * method of class <code>java.lang.Throwable</code>.
   * @param msg the detail message.
   */
  public MalformedCrawlerURLException(String msg)
  {
    super(msg);
  }

  /**
   * Constructs a new <code>MalformedCrawlerURLException</code> exception with the specified 
   * detail message and cause.  
   *
   * @param  message the detail message (which is saved for later retrieval
   *         by the {@link #getMessage()} method).
   * @param  cause the cause (which is saved for later retrieval by the
   *         {@link #getCause()} method).  (A <tt>null</tt> value is
   *         permitted, and indicates that the cause is nonexistent or
   *         unknown.)
   */
  public MalformedCrawlerURLException(String message, Throwable cause)
  {
    super(message, cause);
  }

  /**
   * Constructs a new <code>MalformedCrawlerURLException</code> exception with the specified cause and a detail
   * message of <tt>(cause==null ? null : cause.toString())</tt>.
   *
   * @param  cause the cause (which is saved for later retrieval by the
   *         {@link #getCause()} method).  (A <tt>null</tt> value is
   *         permitted, and indicates that the cause is nonexistent or
   *         unknown.)
   */
  public MalformedCrawlerURLException(Throwable cause)
  {
    super(cause);
  }

}

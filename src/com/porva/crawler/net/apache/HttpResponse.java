/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.net.apache;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.net.DefaultResponseImpl;

/**
 * Response object to present Apache HTTP client get method results.
 * 
 * @author Poroshin V.
 * @date Sep 26, 2005
 */
class HttpResponse extends DefaultResponseImpl
{
  /**
   * Constructs a new {@link HttpResponse} based on <b>executed and not released</b>
   * <code>getMethod</code>, returned by server <code>statusCode</code> and limit of content to
   * store <code>contentLimit</code>.
   * 
   * @param getMethod <b>executed and not released</b> <code>getMethod</code>.
   * @param statusCode http status code returned by server.
   * @param contentLimit limit of content in bytes to store.
   * @param crawlerURL
   * @throws IOException if some I/O failier occurs.
   * @throws NullArgumentException if <code>getMethod</code> of <code>crawlerURL</code> is
   *           <code>null</code>.
   */
  public HttpResponse(final GetMethod getMethod,
                      int statusCode,
                      int contentLimit,
                      final CrawlerURL crawlerURL) throws IOException
  {
    if (getMethod == null)
      throw new NullArgumentException("getMethod");
    if (crawlerURL == null)
      throw new NullArgumentException("crawlerURL");

    setCode(statusCode);

    // init headers
    Map<String, String> headers = new HashMap<String, String>();
    for (Header header : getMethod.getResponseHeaders())
      headers.put(header.getName().toLowerCase(), header.getValue());
    setHeaders(headers);

    setBody(getResponseBody(getMethod.getResponseBodyAsStream(), contentLimit));
//    setCrawlerURL(crawlerURL);
    setTimeWhenDisconnected(System.currentTimeMillis());
  }

  /**
   * Returns response body written form <code>instream</code> or <code>null</code> if
   * <code>instream</code> is <code>null</code>.
   */
  private byte[] getResponseBody(InputStream instream, int contentLimit) throws IOException
  {
    if (instream == null)
      return null;

    ByteArrayOutputStream outstream = new ByteArrayOutputStream();
    byte[] buffer = new byte[4096];
    int len;
    int readedLen = 0;
    while (((len = instream.read(buffer)) > 0) && readedLen <= contentLimit) {
      if (readedLen + len > contentLimit) {
        outstream.write(buffer, 0, contentLimit - readedLen);
        break;
      }
      outstream.write(buffer, 0, len);
      readedLen += len;
    }
    outstream.close();
    return outstream.toByteArray();
  }

  private transient String toString;

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    if (toString == null) {
      toString = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
          .appendSuper(super.toString()).toString();
    }
    return toString;
  }

}

/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.net.apache;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpMethodRetryHandler;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.NoHttpResponseException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.net.HttpClient;
import com.porva.crawler.net.HttpClientException;
import com.porva.crawler.net.MalformedCrawlerURLException;
import com.porva.crawler.net.Response;
import com.porva.crawler.service.AbstractWorkerService;
import com.porva.crawler.service.ServiceException;
import com.porva.crawler.service.WorkerService;

/**
 * Service that can execute http get requests. It is based on Apache HTTPClient library.<br>
 * 
 * @author Poroshin V.
 * @date Sep 26, 2005
 */
public class ApacheHttpService extends AbstractWorkerService implements HttpClient
{
  private int responseBodyLimit = 1024 * 1024; // default content limit 1 Mb

  private String useragent = null;

  private Integer maxRedirects = 3;

  private Integer timeout = 5000;

  private Integer maxNumOfConnections = 100;

  private boolean followRedirects = false;

  private static org.apache.commons.httpclient.HttpClient httpClient;

  private Map<String, String> settings;

  private static Log logger = LogFactory.getLog(ApacheHttpService.class);

  static {
    // Create an HttpClient with the MultiThreadedHttpConnectionManager.
    // This connection manager must be used if more than one thread will
    // be using the HttpClient.
    httpClient = new org.apache.commons.httpclient.HttpClient(
        new MultiThreadedHttpConnectionManager());
  }

  /**
   * Constructs new {@link ApacheHttpService} object with default settings.
   */
  public ApacheHttpService()
  {
    super("workerservice.httpClient");
  }

  /**
   * Constructs new {@link ApacheHttpService} object with specified settings.
   * 
   * @param settings
   * @throws NullArgumentException if <code>settings</code> is <code>null</code>.
   * @throws NumberFormatException if parsing of some number from string has failed.
   */
  public ApacheHttpService(final Map<String, String> settings)
  {
    super("workerservice.httpClient");

    if (settings == null)
      throw new NullArgumentException("settings");
    this.settings = new HashMap<String, String>(settings);

    if (this.settings.containsKey("http.responsebody-limit"))
      responseBodyLimit = Integer.parseInt(settings.get("http.responsebody-limit"));
    if (this.settings.containsKey("http.useragent"))
      useragent = settings.get("http.useragent");
    if (this.settings.containsKey("http.protocol.max-redirects"))
      maxRedirects = Integer.parseInt(settings.get("http.protocol.max-redirects"));
    if (this.settings.containsKey("http.socket.timeout"))
      timeout = Integer.parseInt(settings.get("http.socket.timeout"));
    if (this.settings.containsKey("http.connection-manager.max-total"))
      maxNumOfConnections = Integer.parseInt(settings.get("http.connection-manager.max-total"));
    if (this.settings.containsKey("http.protocol.follow-redirects"))
      followRedirects = Boolean.parseBoolean(settings.get("http.protocol.follow-redirects"));

    httpClient.getParams().setParameter("http.socket.timeout", timeout);
    httpClient.getParams().setParameter("http.protocol.max-redirects", maxRedirects);
    httpClient.getParams().setParameter("http.connection-manager.max-total", maxNumOfConnections);
  }
  
  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.net.HttpClient#get(java.net.URL)
   */
  public Response get(final CrawlerURL url) throws HttpClientException
  {
    if (url == null)
      throw new NullArgumentException("url");
    if (!url.getProtocol().equalsIgnoreCase("http"))
      throw new HttpClientException("Unsupported protocol: " + url.getProtocol());

    GetMethod getMethod = new GetMethod(url.getURLString());
    getMethod.setFollowRedirects(followRedirects);
    getMethod.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
                                       new MyHttpMethodRetryHandler());
    if (useragent != null)
      getMethod.getParams().setParameter("http.useragent", useragent);

    HttpResponse response = null;
    try {
      int statusCode = httpClient.executeMethod(getMethod);
      CrawlerURL crawlerURL = new CrawlerURL(url.getDepth(), getMethod.getURI().toString());
      response = new HttpResponse(getMethod, statusCode, responseBodyLimit, crawlerURL);
    } catch (HttpException e) {
      throw new HttpClientException(e);
    } catch (IOException e) {
      throw new HttpClientException(e);
    } catch (MalformedCrawlerURLException e) {
      throw new HttpClientException(e);
    } finally {
      getMethod.releaseConnection();
      logGetResult(url, response);
    }

    return response;
  }

  private void logGetResult(final CrawlerURL url, Response response)
  {
    if (!logger.isInfoEnabled())
      return;

    StringBuffer sb = new StringBuffer();

    if (response != null) {
      sb.append("HTTP GET complete with code ").append(response.getCode()).append(" : ")
          .append(url.getURLString());
    } else {
      sb.append("HTTP GET failed: ").append(url.getURLString());
    }

    logger.info(sb.toString());
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.service.WorkerService#newCopyInstance()
   */

  public WorkerService newCopyInstance() throws ServiceException
  {
    if (settings == null)
      return new ApacheHttpService();
    return new ApacheHttpService(settings);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
        .appendSuper(super.toString()).append("responseBodyLimit", responseBodyLimit)
        .append("useragent", useragent).append("maxRedirects", maxRedirects).append("timeout",
                                                                                    timeout)
        .append("maxNumOfConnections", maxNumOfConnections).toString();
  }

  // /////////////////////////////////////////////////////////////////////////////////////////

  private static class MyHttpMethodRetryHandler implements HttpMethodRetryHandler
  {
    public boolean retryMethod(final HttpMethod method,
                               final IOException exception,
                               int executionCount)
    {
      if (executionCount >= 3) {
        // Do not retry if over max retry count
        return false;
      }
      if (exception instanceof NoHttpResponseException) {
        // Retry if the server dropped connection on us
        return true;
      }
      if (!method.isRequestSent()) {
        // Retry if the request has not been sent fully or
        // if it's OK to retry methods that have been sent
        return true;
      }
      // otherwise do not retry
      return false;
    }
  }

  public Object getParameter(String paramName)
  {
    return httpClient.getParams().getParameter(paramName);
  }

  public void setParameter(String paramName, Object paramValue)
  {
    httpClient.getParams().setParameter(paramName, paramValue);
  }
};

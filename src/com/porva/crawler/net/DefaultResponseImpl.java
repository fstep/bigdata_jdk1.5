/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.net;

import java.util.HashMap;
import java.util.Map;
import java.util.zip.CRC32;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.porva.crawler.db.DBException;
import com.porva.crawler.db.DBInputStream;
import com.porva.crawler.db.DBOutputStream;
import com.porva.crawler.db.DBValue;

/**
 * Provides default implementation of {@link Response} interface.<br>
 * This class implemented as a wrapper of response values and it is designed to be a superclass for
 * actual implementaion of {@link Response}.
 * 
 * @author Poroshin V.
 * @date Sep 26, 2005
 */
public class DefaultResponseImpl implements Response
{
  private java.util.zip.Checksum checksum = new CRC32();

  private long bodyChecksum = -1;

  private byte[] responseBody = null;

  private Map<String, String> headers = new HashMap<String, String>();

  private int statusCode;

  private long disconnectTime;

  protected DefaultResponseImpl()
  {
  }

  public String getHeaderValue(String name)
  {
    return headers.get(name.toLowerCase());
  }

  public Map<String, String> getHeaders()
  {
    return headers;
  }

  public byte[] getBody()
  {
    return responseBody;
  }

  public int getCode()
  {
    return statusCode;
  }

  public long getTimeWhenDisconnected()
  {
    return disconnectTime;
  }

  public long getBodyChecksum()
  {
    if (getBody() == null)
      return -1;
    if (bodyChecksum == -1) { // lazy initialization
      checksum.reset();
      checksum.update(getBody(), 0, getBody().length);
      bodyChecksum = checksum.getValue();
    }
    return bodyChecksum;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DBValue#writeTo(com.porva.crawler.db.DBOutputStream)
   */
  public void writeTo(DBOutputStream tupleOutput)
  {
    if (tupleOutput == null)
      throw new NullArgumentException("tupleOutput");

    tupleOutput.writeInt(getCode());
    tupleOutput.writeCompressBytes(getBody());
    tupleOutput.writeMapOfStrings(getHeaders());
    tupleOutput.writeLong(getTimeWhenDisconnected());
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DBValue#newInstance(com.porva.crawler.db.DBInputStream)
   */
  public DBValue newInstance(final DBInputStream tupleInput) throws DBException
  {
    if (tupleInput == null)
      throw new NullArgumentException("tupleInput");

    int code = tupleInput.readInt();
    byte[] content = tupleInput.readUncompressBytes();
    Map<String, String> headers = new HashMap<String, String>();
    tupleInput.readMapOfStrings(headers);
    long time = tupleInput.readLong();

    DefaultResponseImpl res = new DefaultResponseImpl();
    res.setCode(code);
    res.setBody(content);
    res.setHeaders(headers);
    res.setTimeWhenDisconnected(time);
    return res;
  }

  public long getClassCode()
  {
    return -108118810741937179L;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  public boolean equals(final Object other)
  {
    if (!(other instanceof DefaultResponseImpl))
      return false;
    Response castOther = (Response) other;
    return new EqualsBuilder().append(getBody(), castOther.getBody())
        .append(getBodyChecksum(), castOther.getBodyChecksum()).append(getHeaders(),
                                                                       castOther.getHeaders())
        .append(getCode(), castOther.getCode()).append(getTimeWhenDisconnected(),
                                                       castOther.getTimeWhenDisconnected())
        .isEquals();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("body.length",
                                                                            responseBody.length)
        .append("body.checksum", getBodyChecksum()).append("headers", headers).append("statusCode",
                                                                                      statusCode)
        .append("disconnectTime", disconnectTime).toString();
  }

  // //////////////////////////////////////////////////////////// protected set methods

  /**
   * Sets disconnection time.
   * 
   * @param disconnectTime disconnection time in milliseconds.
   */
  protected void setTimeWhenDisconnected(long disconnectTime)
  {
    this.disconnectTime = disconnectTime;
  }

  /**
   * Sets response code.
   * 
   * @param code response code.
   */
  protected void setCode(int code)
  {
    this.statusCode = code;
  }

  /**
   * Sets response body.
   * 
   * @param responseBody response body.
   */
  protected void setBody(byte[] responseBody)
  {
    this.responseBody = responseBody;
  }

  /**
   * Sets response headers.
   * 
   * @param headers response headers.
   */
  protected void setHeaders(Map<String, String> headers)
  {
    this.headers = headers;
  }

}

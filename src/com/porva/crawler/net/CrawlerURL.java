/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.net;

import java.net.URI;
import java.net.URL;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.db.DBException;
import com.porva.crawler.db.DBInputStream;
import com.porva.crawler.db.DBOutputStream;
import com.porva.crawler.db.DBValue;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * TODO: current URL rejects some protocols, like nntp. URL that is used for crawling process.<br>
 * 
 * @author Poroshin V.
 * @date Sep 27, 2005
 */
public final class CrawlerURL implements DBValue
{

  // /**
  // * Denotes unknown depth limit.
  // */
  // public static final int UNKNOWN_DEPTH_LIMIT = Integer.MIN_VALUE;

  private String urlString;

  transient private URL url;

  transient private URI uri;

  private int depth;

  private int redirNum;

  // private long[] checksums = new long[0];

  // private int depthLimit = UNKNOWN_DEPTH_LIMIT;

  private static final Log logger = LogFactory.getLog(CrawlerURL.class);

  // /////////////////////////////////////
  // creation methods
  // /////////////////////////////////////

  /**
   * Constuctor for reflective creation in {@link com.porva.crawler.db.CrawlerDBValueFactory}.
   */
  protected CrawlerURL()
  {
  }

  /**
   * Constructs a new {@link CrawlerURL} object with unknown depth limit.<br>
   * 
   * @param depth currect depth of this CrawlerURL object.
   * @param urlStr URL string.
   * @throws NullArgumentException if <code>url</code> is <code>null</code>.
   * @throws MalformedCrawlerURLException if <code>urlStr</code> is invalid url string.
   */
  public CrawlerURL(int depth, final String urlStr) throws MalformedCrawlerURLException
  {
    this(depth, urlStr, 0);
  }

  /**
   * Constructs a new {@link CrawlerURL} object with the same values as given
   * <code>anotherCrawlerURL</code>.<br>
   * 
   * @param anotherCrawlerURL
   * @throws NullArgumentException if <code>anotherCrawlerURL</code> is <code>null</code>.
   */
  public CrawlerURL(final CrawlerURL anotherCrawlerURL)
  {
    if (anotherCrawlerURL == null)
      throw new NullArgumentException("anotherCrawlerURL");

    urlString = anotherCrawlerURL.getURLString();
    url = anotherCrawlerURL.getURL();
    uri = anotherCrawlerURL.getURI();
    depth = anotherCrawlerURL.getDepth();
    redirNum = anotherCrawlerURL.getRedirNum();
  }

  public CrawlerURL(final int depth, final String urlStr, final int redirNum)
      throws MalformedCrawlerURLException
  {
    this.depth = depth;
    this.redirNum = redirNum;

    if (urlStr == null)
      throw new NullArgumentException("urlStr");
    if (this.redirNum < 0)
      throw new IllegalArgumentException("redirNum must be >= 0: " + this.redirNum);

    // todo: optimize
    try {
      this.uri = (new URI(removeFragment(urlStr))).normalize();
      this.url = this.uri.toURL(); // this perfoms encoding of url
      this.urlString = this.url.toExternalForm();
      if (this.url.getPath().equals("")) {
        this.urlString = this.urlString + "/";
        this.uri = new URI(this.urlString);
        this.url = this.uri.toURL();
      }
    } catch (Exception e) {
      throw new MalformedCrawlerURLException(e);
    }
  }
  
  String removeFragment(String urlStr)
  {
    for (int i = urlStr.length() - 1; i > 0; i--) {
      if (urlStr.charAt(i) == '#') {
        return urlStr.substring(0, i);
      }
    }
    return urlStr;
  }

  // ////////////////////////////////////////// getters

  /**
   * Returns URL as String.<br>
   * Returned URL may be different from passed to constructor {@link #CrawlerURL(int, String)} or
   * {@link #CrawlerURL(int, String, int)}. For instance, if constructor's string has path equals
   * to '/' then it will be replaced by empty path string, i.e. "http://localhost/" constructor's
   * string will became "http://localhost".
   * 
   * @return URL as String.
   */
  public String getURLString()
  {
    return urlString;
  }

  /**
   * Returns depth of this crawler url object.
   * 
   * @return depth of this crawler url object.
   */
  public int getDepth()
  {
    return depth;
  }

  /**
   * Returns this object as {@link URL} object.
   * 
   * @return {@link URL} representaion.
   */
  public URL getURL()
  {
    return url;
  }

  /**
   * Returns this object as {@link URI} object.
   * 
   * @return {@link URI} representaion.
   */
  public URI getURI()
  {
    return uri;
  }

  // /**
  // * Returns depth limit of this object.<br>
  // * If depth limit was undefined then {@link #UNKNOWN_DEPTH_LIMIT} will be returned.
  // *
  // * @return depth limit of this object.
  // */
  // public int getDepthLimit()
  // {
  // return depthLimit;
  // }

  /**
   * Returns hostname of url.
   * 
   * @return hostname.
   */
  public String getHost()
  {
    return url.getHost();
  }

  /**
   * Returns path of url or empty string if path part of url does not exist.
   * 
   * @return path of url or empty string if path part of url does not exist.
   */
  public String getPath()
  {
    return url.getPath();
  }

  /**
   * Gets the protocol name.
   * 
   * @return the protocol name.
   */
  public String getProtocol()
  {
    return url.getProtocol();
  }

  /**
   * Gets the port number or -1 if the port is not set.
   * 
   * @return the port number or -1 if the port is not set.
   */
  public int getPort()
  {
    return url.getPort();
  }

  /**
   * Returns redirect num.
   * 
   * @return redirect num.
   */
  public int getRedirNum()
  {
    return redirNum;
  }

  // public long[] getChecksums()
  // {
  // return checksums;
  // }

  // public void setChecksums(long[] checksums)
  // {
  // this.checksums = new long[checksums.length];
  // for (int i = 0; i < checksums.length; i++)
  // this.checksums[i] = checksums[i];
  // }
  //

  /**
   * Constructs a new CrawlerURL by parsing the given string and then resolving it against this
   * CrawlerURL. New resolved CrawlerURL will have also depth == this.crawlerURL.depth + 1.
   * 
   * @param relativeCrawlerURI
   * @return the resulting CrawlerURI object.
   * @throws MalformedCrawlerURLException if <code>relativeCrawlerURI</code> is invalid url
   *           string.
   * @throws NullArgumentException if <code>relativeCrawlerURI</code> is <code>null</code>.
   */
  public CrawlerURL resolveAsChild(final String relativeCrawlerURI)
      throws MalformedCrawlerURLException
  {
    if (relativeCrawlerURI == null)
      throw new NullArgumentException("relativeCrawlerURI");

    CrawlerURL resolvedUrl = null;
    try {
      resolvedUrl = new CrawlerURL(depth + 1, uri.resolve(relativeCrawlerURI).toString());
    } catch (Exception e) {
      throw new MalformedCrawlerURLException(e);
    }

    return resolvedUrl;
  }

  // ////////////////////////////////////
  // implementation of DBValue interface
  // ////////////////////////////////////

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DBValue#writeTo(com.porva.crawler.db.DBOutputStream)
   */
  public void writeTo(final DBOutputStream tupleOutput)
  {
    if (tupleOutput == null)
      throw new NullArgumentException("tupleOutput");

    tupleOutput.writeString(urlString);
    tupleOutput.writeInt(depth);
    tupleOutput.writeInt(redirNum);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DBValue#newInstance(com.porva.crawler.db.DBInputStream)
   */
  public DBValue newInstance(final DBInputStream tupleInput) throws DBException
  {
    if (tupleInput == null)
      throw new NullArgumentException("tupleInput");

    String urlString = tupleInput.readString();
    int depth = tupleInput.readInt();
    int redirNum = tupleInput.readInt();

    try {
      return new CrawlerURL(depth, urlString, redirNum);
    } catch (MalformedCrawlerURLException e) {
      throw new DBException(e);
    }
  }

  public long getClassCode()
  {
    return 3705731374948349L;
  }

  // ////////////////////////////////////
  // Object's methods
  // ////////////////////////////////////

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("urlString",
                                                                              urlString)
        .append("depth", depth).append("redirNum", redirNum).toString();
  }

  public boolean equals(final Object other)
  {
    if (!(other instanceof CrawlerURL))
      return false;
    CrawlerURL castOther = (CrawlerURL) other;
    return new EqualsBuilder().append(urlString, castOther.urlString)
        .append(depth, castOther.depth).append(redirNum, castOther.redirNum).isEquals();
  }

  public int hashCode()
  {
    return new HashCodeBuilder().append(urlString).append(depth).append(redirNum).toHashCode();
  }
}

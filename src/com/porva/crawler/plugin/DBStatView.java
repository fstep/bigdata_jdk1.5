/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.plugin;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Visual representation of {@link DBStat} HtmlViewerControls.<br><br>
 * <img src="../../../../pic/HtmlViewerControls-dbstat.gif">
 */
public class DBStatView  extends JPanel
{
  private static final long serialVersionUID = -5413907090893690486L;
  BorderLayout borderLayout1 = new BorderLayout();
  JScrollPane jScrollPane1 = new JScrollPane();
  JTextArea jTextArea = new JTextArea();

  /**
   * Allocated new {@link DBStatView} object  with string of statistics.
   * @param report string to show.
   */
  protected DBStatView(String report)
  {
    jTextArea.setText(report);
    jTextArea.setEditable(false);
    this.setLayout(borderLayout1);
    this.add(jScrollPane1, BorderLayout.CENTER);
    jScrollPane1.getViewport().add(jTextArea, null);
  }
}

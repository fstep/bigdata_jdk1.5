/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.plugin;

import java.awt.BorderLayout;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JPanel;

import calpa.html.CalHTMLPane;
import calpa.html.CalHTMLPreferences;

import com.porva.crawler.db.CrawlerDBFactory;
import com.porva.crawler.db.DB;
import com.porva.crawler.gui.MainFrame;
import com.porva.crawler.task.FetchResult;
import com.porva.crawler.task.TaskResult;
import com.porva.util.ResourceLoader;

/**
 * Plugin to show html pages like internet browser.<br>
 * This class combines HtmlViewerControls and its visual respresentation together.<br>
 * <br>
 * <img src="../../../../pic/HtmlViewerControls-htmlviewer.gif"><br>
 * HTML viwer is based on <a href="http://www.netcomuk.co.uk/~offshore/index.html">CalPane project</a> .
 */
public class HtmlViewer extends JPanel implements IPlugin
{
  private static final long serialVersionUID = 5462560955786958301L;

  private CalHTMLPane pane;

  HtmlViewerControls controls;

  private BorderLayout borderLayout1 = new BorderLayout();

  private DB<TaskResult> db = null;

  /**
   * Allocates new {@link HtmlViewer} object.
   */
  protected HtmlViewer()
  {
  }

  // regisration of HtmlViewerControls
  static {
    PluginManager.registerPlugin(HtmlViewer.class);
  }

  /**
   * Download given HTML documetn and show it.<br>
   * Warning: this method downloads given url from Interent! Use it for not jet dowloaded pages,
   * i.e. for waiting fetch tasks.
   * 
   * @param uriStr URI of HTML document to download and show.
   */
  public void downloadAndShowHtmlDocument(String uriStr)
  {
    URL url = null;
    try {
      url = new URL(uriStr);
    } catch (MalformedURLException e) {
      // todo: log error
      return;
    }

    controls.setAddressStr(uriStr);
    pane.showHTMLDocument(url);
  }

  /**
   * Retrieves specified by key <code>uriStr</code> document from databse {@link FetchResultDB}
   * and showss it.
   * 
   * @param uriStr key in database of document to show.
   */
  public void showHtmlDocumentFromDB(String uri)
  {
    String content;
    TaskResult taskResult = db.get(uri);
    if (taskResult == null)
      content = "Error: url " + uri + " is not presented in database.";
    else if (taskResult instanceof FetchResult) {
      FetchResult fetchResult = (FetchResult) taskResult;
      if (fetchResult.getStatus() != FetchResult.Status.COMPLETE)
        content = "Status: " + fetchResult.getStatus();
      else
        content = new String(fetchResult.getResponse().getBody());
    } else
      content = "Task in not FetchTask";

    controls.setAddressStr("database: " + uri);
    pane.showHTMLDocument(content);
  }

  public void showHtmlDocument(String name, String htmlString)
  {
    controls.setAddressStr(name);
    pane.showHTMLDocument(htmlString);
  }

  public void showHtmlDocument(URL url)
  {
    pane.showHTMLDocument(url);
    controls.setAddressStr(url.toString());
  }

  /**
   * Run HtmlViewerControls. This method uses {@link PluginManager} class to access different
   * registered objects (objects impelemted {@link PluginUsable}) of application.
   * 
   * @throws Exception
   */
  public void run() throws Exception
  {
    MainFrame mainFrame = (MainFrame) PluginManager.getObject(MainFrame.class);
    CrawlerDBFactory dbFactory = (CrawlerDBFactory) PluginManager.getObject(CrawlerDBFactory.class);
    if (dbFactory != null) {
      db = dbFactory.newResultDatabaseHandle();
      db.open();
    }

    // create a Preferences object
    CalHTMLPreferences pref = new CalHTMLPreferences();

    // use one of its methods to enable the test navbar
    // pref.setShowTestNavBar(true); // todo:

    pane = new CalHTMLPane(pref, null, null);
    controls = new HtmlViewerControls(pane);
    pane
        .showHTMLDocument("<html><body><b>Select link in complete table to see its HTML page.</b></body></html>");
    setLayout(borderLayout1);
    this.add(pane, BorderLayout.CENTER);
    this.add(controls, BorderLayout.SOUTH);

    mainFrame.jPluginsTabbedPane.addTab("HTML viewer", ResourceLoader
        .getIcon("Menu.Plugins.HtmlViewer"), this);
    mainFrame.jPluginsTabbedPane.setSelectedComponent(this);
  }

  public void close() throws Exception
  {
    if (db != null)
      db.close();
  }

}

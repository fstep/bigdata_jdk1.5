/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.plugin;

import java.util.ArrayList;
import java.util.List;

import com.porva.crawler.db.CrawlerDBFactory;
import com.porva.crawler.db.DB;
import com.porva.crawler.db.DBFunctor;
import com.porva.crawler.gui.MainFrame;
import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.task.FetchURLResult;
import com.porva.crawler.task.TaskResult;
import com.porva.crawler.task.FetchResult.Status;
import com.porva.html.HTMLSAXLinksExtractor;
import com.porva.util.ResourceLoader;
import com.sleepycat.je.DatabaseException;

/**
 * Extracts hyperlink graph structure and shows it as tree view.
 */
public class HyperGraph implements IPlugin
{

  private DB<TaskResult> db = null;

  /**
   * Allocates new {@link DBStat} object.
   */
  protected HyperGraph()
  {
  }

  private List<List<String>> perform() throws DatabaseException
  {
    ResultDbFunctor hyperGraphBuilder = new ResultDbFunctor();
    db.iterateOverAll(hyperGraphBuilder);
    List<List<String>> res = hyperGraphBuilder.getHyperGraph();
    return res;
  }

  /**
   * Run HtmlViewerControls. This method uses {@link PluginManager} class to access different
   * registered objects (objects impelemted {@link PluginUsable}) of application.
   * 
   * @throws Exception
   */
  public void run() throws Exception
  {
    CrawlerDBFactory dbFactory = (CrawlerDBFactory) PluginManager.getObject(CrawlerDBFactory.class);
    db = dbFactory.newResultDatabaseHandle();
    db.open();

    List<List<String>> res = perform();

    HyperGraphView view = new HyperGraphView(res);
    MainFrame mainFrame = (MainFrame) PluginManager.getObject(MainFrame.class);
    mainFrame.jPluginsTabbedPane.addTab("Hyperlink Graph", ResourceLoader
        .getIcon("Menu.Plugins.SelfRefMap"), view);
    mainFrame.jPluginsTabbedPane.setSelectedComponent(view);
  }

  public void close() throws Exception
  {
    db.close();

  }

  // ///////////////////////////////////////////////////////////////////
  static class ResultDbFunctor implements DBFunctor<TaskResult>
  {
    private List<List<String>> res = new ArrayList<List<String>>();

    private HTMLSAXLinksExtractor linkExtractor = new HTMLSAXLinksExtractor();

    private List<CrawlerURL> listBuff = new ArrayList<CrawlerURL>();

    public List<List<String>> getHyperGraph()
    {
      return res;
    }

    public void process(final String key, final TaskResult value)
    {
      if (value instanceof FetchURLResult) {
        FetchURLResult fetchResult = (FetchURLResult) value;
        if (fetchResult.getStatus() == Status.COMPLETE) {
          linkExtractor.extractLinks(fetchResult, listBuff);
          
          List<String> links = new ArrayList<String>();
          links.add(fetchResult.getTask().getCrawlerURL().getURLString());
          for (CrawlerURL curl : listBuff)
            links.add(curl.getURLString());
          res.add(links);
          listBuff.clear();
        }
      }
    }
  }

}

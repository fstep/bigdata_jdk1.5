/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.plugin;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 * Visual representation of {@link HyperGraph} HtmlViewerControls.<br>
 * <br>
 * <img src="../../../../pic/HtmlViewerControls-selfrefmap.gif">
 */

public class HyperGraphView extends JPanel
{
  private static final long serialVersionUID = 2257455920269696329L;

  BorderLayout borderLayout1 = new BorderLayout();

  JScrollPane jScrollPane1 = new JScrollPane();

  JTree jTree1;

  JButton jButton = new JButton("Save hyperlink graph structure ...");

  List<List<String>> data;

  /**
   * Allocates new {@link HyperGraphView} object with {@link java.util.Vector} of data to show.
   * 
   * @param data {@link java.util.Vector} object of pages and theis links.
   */
  protected HyperGraphView(List<List<String>> data)
  {
    this.data = data;
    DefaultMutableTreeNode top = new DefaultMutableTreeNode("Hyperlink Graph");

    for (List<String> links : data) {
      String url_str = (String) links.get(0);

      DefaultMutableTreeNode url_node = new DefaultMutableTreeNode(url_str);
      top.add(url_node);

      if (links.size() > 1) {
        for (int i = 1; i < links.size(); i++) {
          DefaultMutableTreeNode link_node = new DefaultMutableTreeNode((String) links.get(i));
          url_node.add(link_node);
        }
      }
    }
    jTree1 = new JTree(top);

    this.setLayout(borderLayout1);
    // this.setSize(new Dimension(400, 300));
    // jScrollPane1.setPreferredSize(new Dimension(3, 3));
    initSaveButton();
    this.add(jScrollPane1, BorderLayout.CENTER);
    this.add(jButton, BorderLayout.SOUTH);
    jButton.setMnemonic(KeyEvent.VK_S);
    jScrollPane1.getViewport().add(jTree1, null);
  }

  private void initSaveButton()
  {
    final Container parentContainer = this.getParent();

    jButton.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        final JFileChooser fc = new JFileChooser();
        int returnVal = fc.showSaveDialog(parentContainer);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
          File file = fc.getSelectedFile();

          try {
            if (!file.createNewFile()) {
              JOptionPane.showMessageDialog(parentContainer, "Cannot create a new file: "
                  + file.getAbsolutePath(), "Error", JOptionPane.ERROR_MESSAGE);
              return;
            }
            if (!file.isFile()) {
              JOptionPane.showMessageDialog(parentContainer, "Should not be a directory: " + file,
                                            "Error", JOptionPane.ERROR_MESSAGE);
              return;
            }
            if (!file.canWrite()) {
              JOptionPane.showMessageDialog(parentContainer, "File cannot be written: " + file,
                                            "Error", JOptionPane.ERROR_MESSAGE);
              return;
            }

            BufferedWriter out = new BufferedWriter(new FileWriter(file));
            for (List<String> links : data) {
              String url_str = (String) links.get(0);

              out.write(url_str);
              out.write("\n");

              if (links.size() > 1) {
                for (int i = 1; i < links.size(); i++) {
                  out.write("\t");
                  out.write((String) links.get(i));
                  out.write("\n");

                }
              }
            }
            out.close();
          } catch (IOException ex) {
            JOptionPane.showMessageDialog(parentContainer, "Error writing to file: " + file,
                                          "Error", JOptionPane.ERROR_MESSAGE);
          }

        }
      }
    });
  }

}

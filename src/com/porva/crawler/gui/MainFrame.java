/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// todo: crawler status in status bar

package com.porva.crawler.gui;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.CrawlerInit;
import com.porva.crawler.plugin.HtmlViewer;
import com.porva.crawler.plugin.IPlugin;
import com.porva.crawler.plugin.PluginManager;
import com.porva.crawler.plugin.PluginUsable;
import com.porva.crawler.plugin.SourceViewer;
import com.porva.util.ResourceLoader;
import com.porva.util.gui.OpenFileFilter;

/**
 * The main Frame of the crawler GUI. 
 *
 * @author Poroshin V.
 */
public class MainFrame extends MainFrameGUI implements PluginUsable
{
  private static final long serialVersionUID = 5520232661305476596L;

  CrawlerMain_Visual adaptee;

  MainFrame_MenuBar jMainFrameMenuBar;

  MainFrame_ToolBar jMainFrameToolBar;

  DialogAbout jDialogAbout;

  MainFrame_StatusBar statusBar;

  private static final String onlineManual = "http://cosco.hiit.fi/search/searchwiki/crawler/";

  private File curDir = null;

  private static Log logger = LogFactory.getLog(MainFrame.class);

  // Construct the frame
  public MainFrame(CrawlerMain_Visual crawlerMain_visual)
  {
    this.adaptee = crawlerMain_visual;
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);

    jMainFrameMenuBar = new MainFrame_MenuBar(this);
    jMainFrameToolBar = new MainFrame_ToolBar(this);
    statusBar = new MainFrame_StatusBar();

    this.setSize(new Dimension(801, 637));
    this.setIconImage(ResourceLoader.getImage("Logo"));
    this.setTitle("Web Crawler - [empty]");
    jSplitPane1.setContinuousLayout(true);
    jSplitPane1.setLastDividerLocation(400);
    jSplitPane1.setResizeWeight(1.0);
    jSplitPane2.setMinimumSize(new Dimension(200, 100));
    jSplitPane2.setContinuousLayout(true);
    jSplitPane2.setResizeWeight(0.0);
    jCompleteTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    ListSelectionModel completeRowSM = jCompleteTable.getSelectionModel();
    completeRowSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        jCompleteTable_selectionChanged(e);
      }
    });

    jWaitingTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    ListSelectionModel waitingRowSM = jWaitingTable.getSelectionModel();
    waitingRowSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        jWaitingTable_selectionChanged(e);
      }
    });

    this.getContentPane().add(statusBar, BorderLayout.SOUTH);
    this.getContentPane().add(jMainFrameToolBar, BorderLayout.NORTH);
    this.setJMenuBar(jMainFrameMenuBar);
    jSplitPane1.setDividerLocation(400);
    jSplitPane2.setDividerLocation(220);

    // statusBar.update(null, adaptee);

    registerObject();
  }

  /**
   * File -> New Project... Shows new project dialog to create a new project.
   * 
   * @param e ActionEvent
   */
  public void actionNewProject(final ActionEvent e)
  {
    final DialogNewProject dialogNewProject = new DialogNewProject(this);
    dialogNewProject.setSize(600, 450);
    dialogNewProject.setVisible(true);
  }

  // Help | About action performed
  public void actionHelpAbout(ActionEvent e)
  {
    jDialogAbout = new DialogAbout(this);
    Dimension dlgSize = jDialogAbout.getPreferredSize();
    Dimension frmSize = getSize();
    Point loc = getLocation();
    jDialogAbout.setLocation((frmSize.width - dlgSize.width) / 2 + loc.x,
                             (frmSize.height - dlgSize.height) / 2 + loc.y);
    jDialogAbout.setModal(true);
    jDialogAbout.pack();
    jDialogAbout.setVisible(true);
  }

  // Overridden so we can exit when window is closed
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    if (e.getID() == WindowEvent.WINDOW_CLOSING) {
      actionFileExit(null);
    }
  }

  // Menu -> Download -> start
  public void actionDownloadStart(ActionEvent e)
  {
    jMainFrameMenuBar.jMenuDownloadStart.setEnabled(false);
    jMainFrameMenuBar.jMenuDownloadPause.setEnabled(true);
    jMainFrameMenuBar.jMenuDownloadSuspend.setEnabled(true);
    jMainFrameMenuBar.jMenuDownloadStart.setText("Resume");

    jMainFrameToolBar.jButtonPlay.setEnabled(false);
    jMainFrameToolBar.jButtonPause.setEnabled(true);
    jMainFrameToolBar.jButtonStop.setEnabled(true);
    jMainFrameToolBar.jButtonPlay.setToolTipText("Resume");

    adaptee.startDownload();
  }

  // Menu -> Download -> Suspend
  public void actionDownloadSuspend(ActionEvent e)
  {
    jMainFrameMenuBar.jMenuDownloadStart.setEnabled(false);
    jMainFrameMenuBar.jMenuDownloadPause.setEnabled(false);
    jMainFrameMenuBar.jMenuDownloadSuspend.setEnabled(false);

    jMainFrameToolBar.jButtonPlay.setEnabled(false);
    jMainFrameToolBar.jButtonPause.setEnabled(false);
    jMainFrameToolBar.jButtonStop.setEnabled(false);

    adaptee.suspendDownload();
  }

  // Menu -> Download -> Pause
  public void actionDownloadPause(ActionEvent e)
  {
    jMainFrameMenuBar.jMenuDownloadStart.setEnabled(true);
    jMainFrameMenuBar.jMenuDownloadPause.setEnabled(false);
    jMainFrameMenuBar.jMenuDownloadSuspend.setEnabled(true);

    jMainFrameToolBar.jButtonPlay.setEnabled(true);
    jMainFrameToolBar.jButtonPause.setEnabled(false);
    jMainFrameToolBar.jButtonStop.setEnabled(true);

    adaptee.pauseDonwload();
  }

  public void actionDownloadResume(ActionEvent e)
  {
    jMainFrameMenuBar.jMenuDownloadStart.setEnabled(false);
    jMainFrameMenuBar.jMenuDownloadPause.setEnabled(true);
    jMainFrameMenuBar.jMenuDownloadSuspend.setEnabled(true);

    jMainFrameToolBar.jButtonPlay.setEnabled(false);
    jMainFrameToolBar.jButtonPause.setEnabled(true);
    jMainFrameToolBar.jButtonStop.setEnabled(true);

    adaptee.resumeDownload();
  }

  // Menu -> File -> Open
  public void actionFileOpenProject(ActionEvent e)
  {
    // 1. get project filename
    JFileChooser chooser = new JFileChooser();
    if (curDir != null)
      chooser.setCurrentDirectory(curDir);
    else {
      File defDir = new File(System.getProperty("user.dir") + File.separator + "projects");
      if (defDir.exists())
        chooser.setCurrentDirectory(defDir);
    }

    OpenFileFilter filter = new OpenFileFilter();
    filter.addExtension("crp");
    filter.setDescription("Crawler project files");
    chooser.setFileFilter(filter);

    if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
      String projectFileName = new String(chooser.getSelectedFile().toString());

      // if (Configuration.searchResouce(projectFileName) == null) {
      // JOptionPane.showMessageDialog(this,
      // "Cannot load " + projectFileName + ". The file does not exist.",
      // "Error",
      // JOptionPane.ERROR_MESSAGE);
      // return ;
      // }
      // if (!Configuration.isValidResource(projectFileName)) {
      // JOptionPane.showMessageDialog(this,
      // "Project file corrupt.",
      // "Cannot load project",
      // JOptionPane.ERROR_MESSAGE);
      // return;
      // }

      // Configuration.addConfResource(projectFileName);
      // Configuration.addConfResource("project.file", projectFileName);
      try {
        InputStream fin = new FileInputStream(projectFileName);
        CrawlerInit.getProperties().load(fin);
        fin.close();
      } catch (IOException ex) {
        JOptionPane.showMessageDialog(this, "Failed to read project file: " + ex.toString(),
                                      "Error", JOptionPane.ERROR_MESSAGE);
        return;
      }

      // CrawlerInit.getProperties().put("project.file", projectFileName);
      curDir = new File(chooser.getCurrentDirectory().toString());
    } else {
      return;
    }
    openProject();
  }

  void openProject()
  {
    // 2. init crawler
    try {
      adaptee.openProject();
    } catch (Exception e1) {
      logger.error(e1);
      e1.printStackTrace();
      return;
    }

    jMainFrameMenuBar.jMenuFileOpenProject.setEnabled(false);
    jMainFrameMenuBar.jMenuFileNewProject.setEnabled(false);
    jMainFrameMenuBar.jMenuFileExportProject.setEnabled(true);
    jMainFrameMenuBar.jMenuFileCloseProject.setEnabled(true);
    jMainFrameMenuBar.jMenuDownloadStart.setEnabled(true);
    jMainFrameMenuBar.jMenuDownloadPause.setEnabled(false);
    jMainFrameMenuBar.jMenuDownloadSuspend.setEnabled(false);
    jMainFrameMenuBar.jMenuPlugins.setEnabled(true);
    jMainFrameMenuBar.jMenuProjectSettings.setEnabled(true);

    jMainFrameToolBar.jButtonPlay.setEnabled(true);
    jMainFrameToolBar.jButtonPause.setEnabled(false);
    jMainFrameToolBar.jButtonStop.setEnabled(false);
    jMainFrameToolBar.jButtonNewProject.setEnabled(false);
    jMainFrameToolBar.jButtonOpenProject.setEnabled(false);
    jMainFrameToolBar.jButtonExportProject.setEnabled(true);
    jMainFrameToolBar.jButtonProjectSettings.setEnabled(true);

    // TableColumnModel tcm = jCompleteTable.getColumnModel();
    // tcm.getColumn(0).setCellRenderer(new RenderRedGreen());
  }

  public void actionFileCloseProject(ActionEvent e)
  {
    adaptee.closeProject();
    downloadDone();

    jMainFrameMenuBar.jMenuFileNewProject.setEnabled(true);
    jMainFrameMenuBar.jMenuFileOpenProject.setEnabled(true);
    jMainFrameMenuBar.jMenuFileExportProject.setEnabled(false);
    jMainFrameMenuBar.jMenuPlugins.setEnabled(false);
    jMainFrameMenuBar.jMenuFileCloseProject.setEnabled(false);
    jMainFrameMenuBar.jMenuProjectSettings.setEnabled(false);
    jMainFrameToolBar.jButtonNewProject.setEnabled(true);
    jMainFrameToolBar.jButtonOpenProject.setEnabled(true);
    jMainFrameToolBar.jButtonExportProject.setEnabled(false);
    jMainFrameToolBar.jButtonProjectSettings.setEnabled(false);

    QueueTableModel emptyTableModel = new QueueTableModel(new Vector());
    jWaitingTable.setModel(emptyTableModel);
    jRunningTable.setModel(emptyTableModel);
    jCompleteTable.setModel(emptyTableModel);
    jPluginsTabbedPane.removeAll();
    ((CommonLogTableModel) jCommonLogTable.getModel()).clear();
    ((ThreadLogTableModel) jThreadLogTable.getModel()).clear();
    ((ThreadLogTableModel) jThreadLogTable.getModel()).fireTableDataChanged();
    ((CommonLogTableModel) jCommonLogTable.getModel()).fireTableDataChanged();

    //statusBar.update(CrawlerStatus.READY, adaptee);
    statusBar.clear();
  }

  public void actionExportProject(ActionEvent e)
  {
    DialogExport dialogExport = new DialogExport(this, adaptee);
    dialogExport.setSize(400, 300);
    dialogExport.setVisible(true);
  }

  public void downloadDone()
  {
    jMainFrameMenuBar.jMenuDownloadStart.setEnabled(false);
    jMainFrameMenuBar.jMenuDownloadStart.setText("Start");
    jMainFrameMenuBar.jMenuDownloadPause.setEnabled(false);
    jMainFrameMenuBar.jMenuDownloadSuspend.setEnabled(false);

    jMainFrameToolBar.jButtonPlay.setEnabled(false);
    jMainFrameToolBar.jButtonPlay.setToolTipText("Start");
    jMainFrameToolBar.jButtonPause.setEnabled(false);
    jMainFrameToolBar.jButtonStop.setEnabled(false);
  }

  // File | Exit action performed
  public void actionFileExit(ActionEvent e)
  {
    adaptee.exit();
  }

  // Settings -> Project settings
  public void actionProjectSettings(ActionEvent e)
  {
    DialogSettings dialogSettingsCurrent = new DialogSettingsCurrent(this, false);
    dialogSettingsCurrent.setSize(415, 525);
    dialogSettingsCurrent.setVisible(true);
  }

  // Settings -> Default Project settings
  public void actionProjectDefaultSettings(ActionEvent e)
  {
    DialogSettings defaultSettingsDialog = new DialogSettingsDefault(this);
    defaultSettingsDialog.setSize(415, 525);
    defaultSettingsDialog.setVisible(true);
  }

  public void actionHelpTopics(ActionEvent e)
  {
    try {
      // todo: problem: cannot show online help first time -- need to press button Go ????
      URL url = new URL(onlineManual);
      HtmlViewer htmlViewer = (HtmlViewer) PluginManager.getPluginActiveObject(HtmlViewer.class);
      if (htmlViewer == null) {
        PluginManager.registerPlugin(HtmlViewer.class);
        htmlViewer = (HtmlViewer) PluginManager.runPlugin(HtmlViewer.class);
      }

      htmlViewer.showHtmlDocument(url.toString(),
                                  "<html>Press button <b>Go</b> to naviagate online help.</html>");
      htmlViewer.showHtmlDocument(url);
    } catch (Exception e1) {
      e1.printStackTrace();
    }
  }

  public void jCompleteTable_selectionChanged(ListSelectionEvent e)
  {
    if (jPluginsTabbedPane.getSelectedComponent() instanceof HtmlViewer) {
      if (e.getValueIsAdjusting())
        return;
      ListSelectionModel lsm = (ListSelectionModel) e.getSource();
      if (lsm.isSelectionEmpty()) // no rows are selected
        return;

      int selectedRow = lsm.getMinSelectionIndex();
      IPlugin iPlugin = PluginManager.getPluginActiveObject(HtmlViewer.class);
      if ((iPlugin == null) || !(iPlugin instanceof HtmlViewer)) {
        logger.fatal("There is no any active HtmlViewer HtmlViewerControls objects!");
        return;
      }

      // at this point we have number of selected column
      // let's retriev content from database and show it
      Object task = jCompleteTable.getValueAt(selectedRow, 0);
      if (task instanceof String)
        ((HtmlViewer) iPlugin).showHtmlDocumentFromDB((String) task);
    }

    else if (jPluginsTabbedPane.getSelectedComponent() instanceof SourceViewer) {
      if (e.getValueIsAdjusting())
        return;
      ListSelectionModel lsm = (ListSelectionModel) e.getSource();
      if (lsm.isSelectionEmpty()) // no rows are selected
        return;

      int selectedRow = lsm.getMinSelectionIndex();
      IPlugin iPlugin = PluginManager.getPluginActiveObject(SourceViewer.class);
      if ((iPlugin == null) || !(iPlugin instanceof SourceViewer)) {
        logger.fatal("There is no any active SourceViewer HtmlViewerControls objects!");
        return;
      }

      // at this point we have number of selected column
      // let's retriev content from database and show it
      Object task = jCompleteTable.getValueAt(selectedRow, 0);
      if (task instanceof String)
        ((SourceViewer) iPlugin).showHtmlSource((String) task);

      // System.out.println("time to show HTML source");
    }
  }

  public void jWaitingTable_selectionChanged(ListSelectionEvent e)
  {
    // if (jPluginsTabbedPane.getSelectedComponent() instanceof HtmlViewer) {
    // if (e.getValueIsAdjusting())
    // return;
    // ListSelectionModel lsm = (ListSelectionModel) e.getSource();
    // if (lsm.isSelectionEmpty()) // no rows are selected
    // return;
    //
    // int selectedRow = lsm.getMinSelectionIndex();
    // IPlugin iPlugin = PluginManager.getPluginActiveObject(HtmlViewer.class);
    // if ((iPlugin == null) || !(iPlugin instanceof HtmlViewer)) {
    // logger.severe("There is no any active HtmlViewer HtmlViewerControls objects!");
    // return;
    // }
    //
    // // at this point we have number of selected column
    // // let's retriev content from database and show it
    // Object task = jWaitingTable.getValueAt(selectedRow, 0);
    // if (task instanceof String)
    // ((HtmlViewer) iPlugin).downloadAndShowHtmlDocument((String) task);
    // else if (task instanceof FetchTask)
    // ((HtmlViewer) iPlugin).downloadAndShowHtmlDocument(((FetchTask) task).getURIString());
    // }
  }

  public void registerObject()
  {
    PluginManager.registerObject(this, this.getClass());
  }

}

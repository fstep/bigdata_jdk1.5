/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// fixed

package com.porva.crawler.gui;

import java.util.logging.Logger;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.Crawler.CrawlerStatus;
import com.porva.crawler.db.CrawlerDBFactory;
import com.porva.crawler.db.DBException;
import com.porva.crawler.manager.DBManager;
import com.porva.crawler.task.InitialTaskReader;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskResult;

/**
 * Manager of the crawle based on {@link DBManager} with support of GUI.
 * 
 * @author Poroshin V.
 * @date Jan 22, 2006
 */
public class CrawlerManager_Visual extends DBManager
{
  CrawlerMain_Visual adaptee = null;

  private LockDialog lockDialog = null;

  private static Logger logger = Logger.getLogger("com.porva.crawler.gui.CrawlerManager_Visual");

  /**
   * Creates a new {@link CrawlerManager_Visual} object.
   * 
   * @param crawlerMain
   * @param taskReader
   * @param dbFactory
   * @throws DBException if some database exception occured.
   * @throws NullArgumentException if <code>crawlerMain</code> is <code>null</code>.
   */
  public CrawlerManager_Visual(final CrawlerMain_Visual crawlerMain,
                               final InitialTaskReader taskReader,
                               final CrawlerDBFactory dbFactory) throws DBException
  {
    super(taskReader, dbFactory);
    adaptee = crawlerMain;
    if (adaptee == null)
      throw new NullArgumentException("adaptee");
  }

  public void completeTask(final Task task, final TaskResult result)
  {
    super.completeTask(task, result);

    adaptee.waitingTableModel.fireTableDataChanged();
    adaptee.completeTableModel.fireTableDataChanged();
    adaptee.runningTableModel.fireTableDataChanged();
    adaptee.frame.statusBar.update(null, adaptee);

    logger.info("Completed " + task.toString());
  }

  /**
   * Used to fire {@link CrawlerStatus} status changing of {@link com.porva.crawler.Crawler} object.
   * 
   * @param newStatus new {@link CrawlerStatus} status that has {@link com.porva.crawler.Crawler}
   *          object.
   */
  public void crawlerStatusChanged(CrawlerStatus newStatus)
  {
    super.crawlerStatusChanged(newStatus);

    // if (newStatus == CrawlerStatus.PAUSING) {
    // lockDialog = new LockDialog(adaptee.frame, "Pausing ...");
    // } else if (newStatus == CrawlerStatus.PAUSED) {
    // lockDialog.close();
    // } else if (newStatus == CrawlerStatus.INITIALIZING) {
    // lockDialog = new LockDialog(adaptee.frame, "Initializing ...");
    // } else if (newStatus == CrawlerStatus.RUNNING) {
    // lockDialog.close();
    // } else if (newStatus == CrawlerStatus.SUSPENDING) {
    // lockDialog = new LockDialog(adaptee.frame, "Suspending ...");
    // } else if (newStatus == CrawlerStatus.SUSPENDED) {
    // lockDialog.close();
    // }
    logger.info("Status changed to " + newStatus);

    if (newStatus == CrawlerStatus.DONE)
      adaptee.frame.downloadDone();

    adaptee.frame.statusBar.update(newStatus, adaptee);
  }
}

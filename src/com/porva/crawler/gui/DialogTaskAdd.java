/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.net.MalformedCrawlerURLException;
import com.porva.crawler.task.FetchTask;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskFactory;
import com.porva.util.gui.JDialogStandart;

/**
 * GUI dialog to create new {@link com.porva.crawler.task.FetchResult} objects and add them to
 * waiting pool.<br>
 * Currently only {@link FetchTask} and {@link GoogleTask} tasks can be created.
 */
class DialogTaskAdd extends DialogAddEditTaskGUI
{
  private static final long serialVersionUID = 8683419111032025380L;

  private AbstractTableModel tableModel = null;

  /**
   * Creates and shows a new {@link DialogTaskAdd} dialog. 
   * 
   * @param owner parent frame.
   * @param tableModel
   */
  public DialogTaskAdd(Frame owner, AbstractTableModel tableModel)
  {
    super(owner);
    super.setTitle("Add task");
    super.setModal(true);
    this.tableModel = tableModel;
    pack();
    JDialogStandart.centerInParent(this);

    jTextFieldURL.setText("http://");
    jTextFieldRSSURL.setText("http://");

    okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonOk_actionPerformed(e);
      }
    });
    getRootPane().setDefaultButton(okButton);

    cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonCancel_actionPerformed(e);
      }
    });
    AbstractAction cancelAction = new AbstractAction()
    {
      private static final long serialVersionUID = -6571333581511840527L;

      public void actionPerformed(ActionEvent e)
      {
        jButtonCancel_actionPerformed(e);
      }
    };
    JDialogStandart.addCancelByEscapeKey(this.getRootPane(), cancelAction);

  }

  public void jButtonOk_actionPerformed(ActionEvent e)
  {
    try {
      Task task = null;
      if (jTabbedPane.getSelectedComponent() == panelTaskFetchTask) {
        String url = jTextFieldURL.getText();
        int depth = ((Integer) jSpinnerDepthFetchTask.getValue()).intValue();
        task = TaskFactory.newFetchURL(new CrawlerURL(depth, url));
      } else if (jTabbedPane.getSelectedComponent() == panelTaskGoogleTask) {
        String query = jTextFieldQuery.getText();
        int depth = ((Integer) jSpinnerDepthGoogleTask.getValue()).intValue();
        int hitsNum = ((Integer) jSpinnerHitsNum.getValue()).intValue();
        task = TaskFactory.newGoogleTask(hitsNum, query, depth);
      } else if (jTabbedPane.getSelectedComponent() == panelTaskFetchRssTask) {
        String url = jTextFieldRSSURL.getText();
        int depth = ((Integer) jSpinnerDepthFetchRSSTask.getValue()).intValue();
        task = TaskFactory.newFetchRSS(new CrawlerURL(depth, url));
      }
      
      tableModel.setValueAt(task, tableModel.getRowCount(), 0);
      tableModel.fireTableDataChanged();
    } catch (MalformedCrawlerURLException ex) {
      JOptionPane
          .showMessageDialog(this, ex.getMessage(), "Invalid url", JOptionPane.ERROR_MESSAGE);
      return;
    }

    this.dispose();
  }

  /**
   * Cancel button or 'escape' reaction.
   * 
   * @param e
   */
  public void jButtonCancel_actionPerformed(ActionEvent e)
  {
    this.dispose();
  }

}

/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.UIManager;

import com.jgoodies.plaf.FontSizeHints;
import com.jgoodies.plaf.LookUtils;
import com.jgoodies.plaf.Options;
import com.jgoodies.plaf.plastic.PlasticLookAndFeel;
import com.jgoodies.plaf.plastic.theme.SkyBlue;
import com.porva.crawler.Crawler;
import com.porva.crawler.CrawlerInit;
import com.porva.crawler.db.CrawlerDBFactory;
import com.porva.crawler.db.DBException;
import com.porva.crawler.db.DBExporter;
import com.porva.crawler.db.DefaultExportFilter;
import com.porva.crawler.db.ExportFilter;
import com.porva.crawler.service.ServiceException;
import com.porva.util.log.ConsoleFormatter;

/**
 * Main class to start crawer applicaton with GUI.
 */
public class CrawlerMain_Visual
{
  private boolean packFrame = false;

  MainFrame frame;

  QueueTableModel waitingTableModel;

  QueueTableModel runningTableModel;

  QueueTableModel completeTableModel;

//  private CrawlerInit crawlerInit;

  private Crawler crawler;

  File curDir = null; // current directory to store; File -> Open project;

  String projectFileName = null;

  private CommonLogAppender commonLogAppender;

  private ThreadLogAppender threadLogAppender;

  private static Logger logger;

  /**
   * Constucts and starts the main frame of the crawler.<br>
   * This method loads configuration resources and shows main application frame.
   */
  public CrawlerMain_Visual()
  {
    java.util.logging.Logger.getLogger("").setLevel(java.util.logging.Level.INFO);
    java.util.logging.Logger.getLogger("").getHandlers()[0].setFormatter(new ConsoleFormatter());
    logger = Logger.getLogger("com.porva.crawler.gui.CrawlerManager_Visual");

    configureUI();

    frame = new MainFrame(this);

    commonLogAppender = CommonLogAppender.getInstance(frame.jCommonLogTable);
    java.util.logging.Logger.getLogger("").addHandler(commonLogAppender);
    threadLogAppender = ThreadLogAppender.getInstance(frame.jThreadLogTable);
    java.util.logging.Logger.getLogger("com.porva.crawler.CrawlerWorker")
        .addHandler(threadLogAppender);

    if (packFrame) {
      frame.pack();
    } else {
      frame.validate();
    }
    // Center the window
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension frameSize = frame.getSize();
    if (frameSize.height > screenSize.height) {
      frameSize.height = screenSize.height;
    }
    if (frameSize.width > screenSize.width) {
      frameSize.width = screenSize.width;
    }
    frame.setLocation((screenSize.width - frameSize.width) / 2,
                      (screenSize.height - frameSize.height) / 2);
    frame.setVisible(true);
  }

  private void configureUI()
  {
    UIManager.put(Options.USE_SYSTEM_FONTS_APP_KEY, Boolean.TRUE);
    Options.setGlobalFontSizeHints(FontSizeHints.MIXED);
    Options.setDefaultIconSize(new Dimension(18, 18));

    String lafName = null;
    PlasticLookAndFeel.setMyCurrentTheme(new SkyBlue());
    if (LookUtils.IS_OS_WINDOWS_MODERN) {
      lafName = Options.PLASTIC_NAME;
    } else if (LookUtils.IS_OS_WINDOWS) {
      // lafName = Options.EXT_WINDOWS_NAME;
    } else {
      // lafName = Options.getSystemLookAndFeelClassName();
      lafName = Options.PLASTIC_NAME;
    }
    try {
      UIManager.setLookAndFeel(lafName);
    } catch (Exception e) {
      System.err.println("Can't set look & feel:" + e);
    }
  }

  /**
   * Opens a new project.
   * 
   * @throws Exception
   */
  public void openProject() throws Exception
  {

    // constracting crawler
    crawler = CrawlerInit.constructCrawler(CrawlerInit.getProperties(), this);

    waitingTableModel = new QueueTableModel(CrawlerInit.getGUIWorkload().getWaiting());
    runningTableModel = new QueueTableModel(CrawlerInit.getGUIWorkload().getRunning());
    completeTableModel = new QueueTableModel(CrawlerInit.getGUIWorkload().getComplete());

    frame.jWaitingTable.setModel(waitingTableModel);
    frame.jRunningTable.setModel(runningTableModel);
    frame.jCompleteTable.setModel(completeTableModel);
    waitingTableModel.fireTableDataChanged();
    completeTableModel.fireTableDataChanged();
    runningTableModel.fireTableDataChanged();

    frame.statusBar.update(null, this);

    String projectName = (String) CrawlerInit.getProperties().get("project.name");
    frame.setTitle(projectName + " - Web Crawler");

    logger.info("Project is opened.");
  }

  /**
   * Closes opened project.
   */
  public void closeProject()
  {
    CrawlerInit.getProperties().clear();
    try {
      CrawlerInit.finalizeCrawler();
    } catch (DBException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (ServiceException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (Exception e) {
      // TODO: handle exception
      e.printStackTrace();
    }
    frame.setTitle("Web Crawler");

    logger.info("Project is closed.");
  }

  /**
   * Exports project to specified local directory <code>dir</code>.
   * 
   * @param dir local directory to export to.
   * @param isExportFetchURL <code>true</code> to export fetch URL tasks results; <code>false</code> otherwise.
   * @param isExportFetchRSS <code>true</code> to export fetch RSS tasks results; <code>false</code> otherwise.
   * @param isExportGoogle <code>true</code> to export Google tasks resuls; <code>false</code> otherwise. 
   * @param isExportRobotxt <code>true</code> to export robots.txt files; <code>false</code>
   *          otherwise.
   * @throws DBException
   */
  public void exportProject(String dir,
                            boolean isExportFetchURL,
                            boolean isExportFetchRSS,
                            boolean isExportGoogle,
                            boolean isExportRobotxt) throws DBException
  {
    CrawlerDBFactory dbFactory = CrawlerInit.initDBFactory(CrawlerInit.getProperties());

    DBExporter exporter = new DBExporter();

    File f = new File(dir);
    f.mkdir();
    DefaultExportFilter filter = new DefaultExportFilter(null);
    filter.setExportFetchURL(isExportFetchURL);
    filter.setExportGoogle(isExportGoogle);
    filter.setExportRobotxt(isExportRobotxt);
    filter.setExportFetchRSS(isExportFetchRSS);
    List<ExportFilter> filters = new ArrayList<ExportFilter>();
    filters.add(filter);
    exporter.exportNews(dbFactory, f, filters);

    dbFactory.close();
  }

  /**
   * Exits the application.
   */
  public void exit()
  {
    // if (manager != null && !manager.isClosed())
    // manager.close();
    System.exit(0);
  }

  /**
   * Starts downloading process.
   */
  public void startDownload()
  {
    logger.info("Staring crawling process ...");
    crawler.startWork();
  }

  /**
   * Suspends downloading process.
   */
  public void suspendDownload()
  {
    logger.info("Suspending crawling process ...");
    crawler.suspendWork();
  }

  /**
   * Pauses downloading process.
   */
  public void pauseDonwload()
  {
    logger.info("Pausing crawling process ...");
    crawler.pauseWork();
  }

  /**
   * Resumes downloading process.
   */
  public void resumeDownload()
  {
    logger.info("Resuming crawling process ...");
    crawler.resumeWork();
  }

  /**
   * Starts Crawler application with GUI.
   * 
   * @param argv
   */
  public static void main(String[] argv)
  {
    new CrawlerMain_Visual();
  }
}

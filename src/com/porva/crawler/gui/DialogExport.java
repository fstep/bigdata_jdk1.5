/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import com.porva.crawler.CrawlerInit;
import com.porva.crawler.db.DBException;
import com.porva.util.gui.JDialogStandart;

/**
 * Export project dialog.
 */
class DialogExport extends DialogExportGUI
{
  private static final long serialVersionUID = -3667826710681305034L;

  private CrawlerMain_Visual adaptee;

  // private File defExportDir;
  private Frame parentFrame;

  public DialogExport(Frame owner, CrawlerMain_Visual adaptee)
  {
    super(owner);
    super.setModal(true);
    this.parentFrame = owner;
    this.adaptee = adaptee;

    String exportDir = new File((String) CrawlerInit.getProperties().get("project.file"))
        .getParent();
    exportDir += System.getProperty("file.separator") + "export-"
        + Long.toString(System.currentTimeMillis());

    jTextFieldDir.setText(exportDir);

    pack();
    JDialogStandart.centerInParent(this);

    okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonOk_actionPerformed(e);
      }
    });
    getRootPane().setDefaultButton(okButton);

    cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonCancel_actionPerformed(e);
      }
    });

    jButtonChooseDir.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonChooseDir_actionPerformed(e);
      }
    });

    AbstractAction cancelAction = new AbstractAction()
    {
      private static final long serialVersionUID = -6571333581511840527L;

      public void actionPerformed(ActionEvent e)
      {
        jButtonCancel_actionPerformed(e);
      }
    };
    JDialogStandart.addCancelByEscapeKey(this.getRootPane(), cancelAction);
  }

  public void jButtonOk_actionPerformed(ActionEvent e)
  {
    File dir = new File(jTextFieldDir.getText());
    if (!dir.isDirectory()) {
      if (!dir.mkdirs()) {
        JOptionPane.showMessageDialog(this, "Cannot create export directory "
            + jTextFieldDir.getText(), "Export project error", JOptionPane.ERROR_MESSAGE);
        return;
      }
    }

    LockDialog lockDialog = new LockDialog(parentFrame, "Exporting project");
    try {
      adaptee.exportProject(dir.getAbsolutePath(), checkBoxExportFetchURL.isSelected(),
                            checkBoxExportFetchRSS.isSelected(), checkBoxExportGoogle.isSelected(),
                            checkBoxExportRobotxt.isSelected());
    } catch (DBException e1) {
      JOptionPane.showMessageDialog(this, "Error during exporting: " + e1.toString(),
                                    "Export project error", JOptionPane.ERROR_MESSAGE);
      return;
    }
    lockDialog.close();
    this.dispose();
  }

  public void jButtonCancel_actionPerformed(ActionEvent e)
  {
    dispose();
  }

  public void jButtonChooseDir_actionPerformed(ActionEvent e)
  {
    JFileChooser chooser = new JFileChooser();
    chooser.setCurrentDirectory(adaptee.curDir);
    chooser.setDialogTitle("Select export directory");
    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
      jTextFieldDir.setText(chooser.getSelectedFile().getAbsolutePath());
    }
  }

}

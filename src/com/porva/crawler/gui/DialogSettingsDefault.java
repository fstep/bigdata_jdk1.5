/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JOptionPane;

import com.porva.crawler.CrawlerInit;

/**
 * GUI dialog of default project settings.<br>
 * Default project settings is used for every new project.
 */
class DialogSettingsDefault extends DialogSettings
{
  private static final long serialVersionUID = 7273515534332194429L;

  public DialogSettingsDefault(final Frame owner)
  {
    super(owner, CrawlerInit.getDefaultProperties());
    super.setTitle("Default Project properties");
    this.jTabbedPane.setSelectedIndex(1);
    this.jTabbedPane.setEnabledAt(0, false);
  }

  protected void setProp(Properties prop)
  {
    super.setProp(prop);

    jTableTasks.setEnabled(false);
    jButtonAddTask.setEnabled(false);
    jButtonEditTask.setEnabled(false);
    jButtonRemoveTask.setEnabled(false);
  }

  public void jButtonOk_actionPerformed(ActionEvent e)
  {
    // URL defaultConf = Configuration.searchResouce("default.xml");
    // System.out.println(defaultConf.getPath());
    // if (defaultConf == null)
    // //todo: dialog box: cannot save default settings: default xml file is not found!
    // System.err.println("cannot save default settings: default xml file is not found");
    // else
    // try {
    // Configuration.saveConfFile(defaultConf.getPath(), getProp());
    // } catch (IOException e1) {
    // //todo: dialog box: cannot save default settings:
    // System.err.println("cannot save default settings: " + e.toString());
    // }
    // } catch (URISyntaxException e1) {
    // //todo: dialog box: cannot save default settings:
    // System.err.println("cannot save default settings: " + e.toString());
    // }
    try {
      CrawlerInit.getDefaultProperties().putAll(getProp());
      FileOutputStream fout = new FileOutputStream((String) CrawlerInit.defaultPropertiesFile);
      CrawlerInit.getDefaultProperties().store(fout, null);
      fout.close();
    } catch (IOException e1) {
      JOptionPane.showMessageDialog(this, "Failed to save default settings file", "Settings error",
                                    JOptionPane.ERROR_MESSAGE);
      return;
    }

    this.dispose();
  }

}

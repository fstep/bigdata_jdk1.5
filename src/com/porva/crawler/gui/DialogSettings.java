/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.CrawlerInit;
import com.porva.crawler.filter.Filter;
import com.porva.crawler.gui.filter.FilterDialogsFactory;
import com.porva.crawler.gui.filter.ServiceTableModel;
import com.porva.html.DOMDocumentParser;
import com.porva.util.ResourceLoader;
import com.porva.util.gui.JDialogStandart;

/**
 * Skeleton for GUI settings dialog.<br>
 * Settings dialog contain all settings of the project.
 */
abstract class DialogSettings extends DialogSettingsGUI
{
  private Frame owner = null;

  private static Logger logger = Logger.getLogger("com.porva.crawler.gui.DialogSettings");

  private final Icon iconHelpBulb = ResourceLoader.getIcon("Misc.HelpBulb");

  private final ServiceTableModel<Filter> filtersTableModel = new ServiceTableModel<Filter>();

  private final ServiceTableModel<DOMDocumentParser> parsersTableModel = new ServiceTableModel<DOMDocumentParser>();

  public DialogSettings(final Frame owner, Properties prop)
  {
    super(owner);
    super.setModal(true);
    this.owner = owner;

    jComboBoxOrderAlgorithm.addItem("RANDOM"); // todo: add order algorithms
    jComboBoxPredefinedAgents.addItem(PredefinedHttpAgents.NutchCVS);
    jComboBoxPredefinedAgents.addItem(PredefinedHttpAgents.MSIE);

    // todo: enable this text filelds the it will be possible to change
    // default template of paths
    jTextFieldDbDir.setEditable(false);
    jTextFieldName.setEditable(false);
    jTextFieldProjectName.setEditable(false);
    jTextFieldWaitingFile.setEditable(false);
    jTextFieldProjectFile.setEditable(false);

    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    setProp(prop);
    pack();
    JDialogStandart.centerInParent(this);

    // init filters
    ListSelectionModel rowSM = tableFilters.getSelectionModel();
    rowSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        tableColumnSelected(e);
      }
    });

    // assign operations for buttons
    okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonOk_actionPerformed(e);
      }
    });
    getRootPane().setDefaultButton(okButton);

    cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonCancel_actionPerformed(e);
      }
    });

    jComboBoxPredefinedAgents.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jComboBoxPredefinedAgents_actionPerformed(e);
      }
    });

    jButtonEditFilter.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonEditFilter_actionPerformed(e);
      }
    });
    jButtonEditFilter.setMnemonic(KeyEvent.VK_E);
    jButtonEditFilter.setEnabled(false);

    labelHelp.setIcon(iconHelpBulb);
    labelHelp.setText(FiltersTips.getDefaultTip());

    AbstractAction cancelAction = new AbstractAction()
    {
      private static final long serialVersionUID = -6571333581511840527L;

      public void actionPerformed(ActionEvent e)
      {
        jButtonCancel_actionPerformed(e);
      }
    };
    JDialogStandart.addCancelByEscapeKey(this.getRootPane(), cancelAction);
  }

  public void tableColumnSelected(ListSelectionEvent e)
  {
    // Ignore extra messages.
    if (e.getValueIsAdjusting())
      return;

    ListSelectionModel lsm = (ListSelectionModel) e.getSource();
    if (lsm.isSelectionEmpty()) {
      labelHelp.setText("Select row table to see hint for selected filter.");
      jButtonEditFilter.setEnabled(false);
    } else {
      jButtonEditFilter.setEnabled(true);
      int selectedRow = lsm.getMinSelectionIndex();
      Filter selectedFiler = (Filter) filtersTableModel.getServices().get(selectedRow);
      labelHelp.setText(FiltersTips.getTip(selectedFiler.getServiceName()));
    }
  }

  protected void setProp(final Properties prop)
  {
    jTextFieldProjectFile.setText(prop.getProperty("project.file"));
    jTextFieldProjectName.setText(prop.getProperty("project.name"));
    jTextFieldDbDir.setText(prop.getProperty("db.dir"));
    jTextFieldWaitingFile.setText(prop.getProperty("workload.waiting.filename"));

    jSpinnerHttpTimeout.setValue(new Integer(prop.getProperty("http.socket.timeout")));
    jSpinnerContentLimit.setValue(new Integer(prop.getProperty("http.responsebody-limit")));
    jSpinnerMaxRedirects.setValue(new Integer(prop.getProperty("http.protocol.max-redirects")));
    jCheckBoxIsHttp11.setSelected((new Boolean(prop.getProperty("http.version.1.1")))
        .booleanValue());
    jTextFieldProxy.setText(prop.getProperty("http.proxy.host"));
    // Configuration.get("http.port"); // todo: http.port value

    jTextFieldName.setText(prop.getProperty("http.useragent"));
    jTextFieldRobotsAgents.setText(prop.getProperty("http.robots.agents"));
    jTextFieldDescription.setText(prop.getProperty("http.agent.description"));
    jTextFieldUrl.setText(prop.getProperty("http.agent.url"));
    jTextFieldMail.setText(prop.getProperty("http.agent.email"));
    jTextFieldVersion.setText(prop.getProperty("http.agent.version"));
    String agentName = prop.getProperty("http.useragent");
    for (int i = 0; i < jComboBoxPredefinedAgents.getItemCount(); i++) {
      if (jComboBoxPredefinedAgents.getItemAt(i).toString().equals(agentName)) {
        jComboBoxPredefinedAgents.setSelectedIndex(i);
        break;
      }
    }

    jSpinnerServerDelay.setValue(new Integer(prop.getProperty("crawler.server.delay")));
    jSpinnerServerThreads.setValue(new Integer(1));
    jSpinnerdFetcherThreads.setValue(new Integer(prop.getProperty("crawler.workers-num")));
    jSpinnerDepthLimit.setValue(new Integer(prop.getProperty("filter.depth-limit")));
    jSpinnerLifetime.setValue(new Long(prop.getProperty("task.FetchTask.lifetime")));
    // jComboBoxOrderAlgorithm.setSelectedItem(
    // DownloadOrderAlgorithm.initFromString(prop.getProperty("crawler.DownloadOrderAlgorithm")) );

    initFiltersFrom(prop);
    initParsersFrom(prop);
  }

  private void initParsersFrom(final Properties prop)
  {
    // todo
    tableParsers.setModel(parsersTableModel);
    for (Map.Entry<DOMDocumentParser, Boolean> entry : CrawlerInit.getParsers(prop).entrySet())
      parsersTableModel.addService(entry.getKey(), entry.getValue());
    
    tableParsers.getColumnModel().getColumn(0).setMinWidth(20);
    tableParsers.getColumnModel().getColumn(0).setMaxWidth(20);
    tableParsers.getColumnModel().getColumn(1).setMinWidth(130);
    tableParsers.getColumnModel().getColumn(1).setMaxWidth(150);
    tableParsers.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    parsersTableModel.fireTableDataChanged();
  }

  /**
   * Init table view of filters.
   */
  private void initFiltersFrom(final Properties prop)
  {
    tableFilters.setModel(filtersTableModel);
    for (Map.Entry<Filter, Boolean> entry : CrawlerInit.getFilters(prop).entrySet())
      filtersTableModel.addService(entry.getKey(), entry.getValue());

    tableFilters.getColumnModel().getColumn(0).setMinWidth(20);
    tableFilters.getColumnModel().getColumn(0).setMaxWidth(20);
    tableFilters.getColumnModel().getColumn(1).setMinWidth(130);
    tableFilters.getColumnModel().getColumn(1).setMaxWidth(150);
    tableFilters.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    filtersTableModel.fireTableDataChanged();
  }

  protected Properties getProp()
  {
    Properties prop = new Properties();

    prop.put("project.file", jTextFieldProjectFile.getText());
    prop.put("project.name", jTextFieldProjectName.getText());
    prop.put("db.dir", jTextFieldDbDir.getText());
    prop.put("workload.waiting.filename", jTextFieldWaitingFile.getText());

    prop.put("http.socket.timeout", jSpinnerHttpTimeout.getValue().toString());
    prop.put("http.responsebody-limit", jSpinnerContentLimit.getValue().toString());
    prop.put("http.protocol.max-redirects", jSpinnerMaxRedirects.getValue().toString());
    prop.put("http.version.1.1", Boolean.toString(jCheckBoxIsHttp11.isSelected()));

    prop.put("http.useragent", jTextFieldName.getText());
    prop.put("http.robots.agents", jTextFieldRobotsAgents.getText());
    prop.put("http.agent.description", jTextFieldDescription.getText());
    prop.put("http.agent.url", jTextFieldUrl.getText());
    prop.put("http.agent.email", jTextFieldMail.getText());
    prop.put("http.agent.version", jTextFieldVersion.getText());

    prop.put("crawler.server.delay", jSpinnerServerDelay.getValue().toString());
    prop.put("crawler.server.activeThreads", jSpinnerServerThreads.getValue().toString());
    prop.put("crawler.workers-num", jSpinnerdFetcherThreads.getValue().toString());
    prop.put("filter.depth-limit", jSpinnerDepthLimit.getValue().toString());
    prop.put("task.FetchTask.lifetime", jSpinnerLifetime.getValue().toString());
    // prop.put("crawler.DownloadOrderAlgorithm",
    // jComboBoxOrderAlgorithm.getSelectedItem().toString());

    addFiltersTo(prop);

    return prop;
  }

  private void addFiltersTo(final Properties prop)
  {
    assert prop != null;

    for (Filter filter : filtersTableModel.getServices())
      prop.put(filter.getServiceName(), filter.getConfigStr());

    StringBuffer sb = new StringBuffer();
    sb.append("filter.depth-limit,filter.redirects-limit,filter.robots-rules,"); // todo: get this
    // string from
    // cofing ???
    for (Filter filter : filtersTableModel.getCheckedServices())
      sb.append(filter.getServiceName()).append(",");
    String activeFiltersStr = sb.toString();
    if (filtersTableModel.getCheckedServices().size() != 0)
      activeFiltersStr = activeFiltersStr.substring(0, activeFiltersStr.length() - 1);

    prop.put("filters.active-filters", activeFiltersStr);
  }

  abstract public void jButtonOk_actionPerformed(ActionEvent e);

  public void jButtonCancel_actionPerformed(ActionEvent e)
  {
    this.dispose();
  }

  public void jButtonEditFilter_actionPerformed(ActionEvent e)
  {
    int row = tableFilters.getSelectedRow();
    Object obj = filtersTableModel.getServices().get(row);
    if (obj instanceof Filter) {
      JDialog dialog = FilterDialogsFactory.newFilterDialog(owner, (Filter) obj);
      dialog.setSize(300, 400);
      dialog.setVisible(true);
    } else {
      logger.warning("Object is not an instance of Filter!!!");
    }
    filtersTableModel.fireTableDataChanged();
  }

  public void jComboBoxPredefinedAgents_actionPerformed(ActionEvent e)
  {
    JComboBox jComboBox = (JComboBox) e.getSource();
    HttpAgent selectedAgent = (HttpAgent) jComboBox.getSelectedItem();

    jTextFieldName.setText(selectedAgent.name);
    jTextFieldRobotsAgents.setText(selectedAgent.robotsAgents);
    jTextFieldDescription.setText(selectedAgent.desc);
    jTextFieldUrl.setText(selectedAgent.url);
    jTextFieldMail.setText(selectedAgent.mail);
    jTextFieldVersion.setText(selectedAgent.version);
  }

  // /////////////////////////////////////////////////////////////////////

  private static class FiltersTips
  {
    /**
     * Returns default HTML tip that is used then none filter is selected.
     * 
     * @return default HTML tip.
     */
    public static final String getDefaultTip()
    {
      return "<html>All filters are in order of their processing. If any filter classifies task as "
          + " dissalowed then this task will be excluded from crawling process.<br>"
          + "<i>Select row in the table to see hint for specified filter.</i></html>";
    }

    /**
     * Returns HTML help tip for given filter name.
     * 
     * @param filterName name of the filter.
     * @return HTML help tip for given filter name.
     * @throws NullArgumentException if <code>filterName</code> is <code>null</code>.
     */
    public static final String getTip(final String filterName)
    {
      if (filterName == null)
        throw new NullArgumentException("filterName");

      if (filterName == Filter.DEPTH_LIMIT)
        return makeHeader("Depth limit filter")
            + "This filer limits crawling process to defined hyperlink depth.</html>";
      if (filterName == Filter.REDIRECTS_LIMIT)
        return makeHeader("Redirects limit filter")
            + "This filer limits number of redirects to follow by the crawler.</html>";
      if (filterName == Filter.MIME_TYPE)
        return makeHeader("Mime-type filter")
            + "This filer allows to download only recources with specified mime-types.</html>";
      if (filterName == Filter.ROBOTS_RULES)
        return makeHeader("Robots rules filter")
            + "<i>robots.txt</i> file can be located on any server to specify "
            + "what and who can access its resources. It is very unpolite to "
            + "ignore these rules, so it is better to keep this filter always on.</html>";
      if (filterName == Filter.FILE_TYPE)
        return makeHeader("File type filter")
            + "This filter allows to download only files of specified types or "
            + "otherwise to dowload evething expect selected types.<br>"
            + "Select it if you want to crawl, for instance, only html pages.</html>";
      if (filterName == Filter.PROTOCOLS)
        return makeHeader("Schemes filter")
            + "This filter allows to use only specified schemes (protocols) or "
            + "otherwise to avoid any URLs with selected schemes.<br>"
            + "Select it if you want to crawl, for instance, only pages via HTTP.</html>";
      if (filterName == Filter.HOST)
        return makeHeader("Host filter")
            + "This filter locks crawling proccess inside specified hosts (web servers). <br>"
            + "Select it if you want to crawl, for instance, only pages from host <i>java.sun.com</i>.</html>";
      if (filterName == Filter.DOMAINS)
        return makeHeader("Domain filter")
            + "This filter locks crawling proccess inside specified domains or "
            + "otherwise avoids any URLs from selected domains.<br>"
            + "Select it if you want to crawl, for instance, only pages from domains: <i>fi, com, org, net</i>.</html>";
      // if (filterName == Filter.)
      // return header
      // + "This filter limits totla number of downloaded pages and also"
      // + "can specify how many documents can be downloaded per one host.<br>"
      // + "Select it if you want to crawl, for instance, totally only 100 pages and 10 pages per
      // host.";
      // return null;
      return "help is not available";
    }

    private static String makeHeader(final String filterName)
    {
      return "<html>Filter: <b>" + filterName + "</b><br>";
    }
  }

}

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Represents information about http user agent.
 */
class HttpAgent
{
  String name = null;

  String robotsAgents = "* ";

  String desc = " ";

  String url = " ";

  String mail = " ";

  String version = " ";

  public String toString()
  {
    return name;
  }
}

/**
 * Class represents record of predefined http user agents.
 */
class PredefinedHttpAgents
{
  public static HttpAgent NutchCVS = new HttpAgent();

  public static HttpAgent MSIE = new HttpAgent();

  static {
    // Nutch
    NutchCVS.name = "NutchCVS";
    NutchCVS.robotsAgents = "NutchCVS,Nutch,*";
    NutchCVS.desc = "Nutch";
    NutchCVS.url = "http://www.nutch.org/docs/en/bot.html";
    NutchCVS.mail = "nutch-agent@lists.sourceforge.net";
    NutchCVS.version = "0.03-dev";

    // MSEI
    MSIE.name = "Mozilla/4.0";
    MSIE.robotsAgents = "Mozilla/4.0,*";
    MSIE.desc = "compatible; MSIE 4.01; Windows NT; MS Search 4.0 Robot";

  }
}

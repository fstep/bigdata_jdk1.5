/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui.filter;

import com.porva.crawler.filter.*;

import javax.swing.*;

import org.apache.commons.lang.NullArgumentException;

import java.awt.*;

/**
 * Factory of Filters Dialogs.
 */
public class FilterDialogsFactory
{

  private FilterDialogsFactory()
  {
  }

  /**
   * Returns dialog for given filter.
   * 
   * @param owner parent frame.
   * @param filter filter object.
   * @return dialog for given filter.
   * @throws IllegalArgumentException if <code>fliter</code> does not have corresponded dialog.
   * @throws org.apache.commons.lang.NullArgumentException if <code>owner</code> or
   *           <code>filter</code> is <code>null</code>.
   */
  public static JDialog newFilterDialog(final Frame owner, Filter filter)
  {
    if (filter == null)
      throw new NullArgumentException("filter");
    
    String filterType = filter.getServiceName();

    if (filterType == Filter.DOMAINS)
      return new DialogDomainFilter(owner, filter);
    if (filterType == Filter.PROTOCOLS)
      return new DialogShcemeFilter(owner, filter);
    if (filterType == Filter.MIME_TYPE)
      return new DialogMimeTypeFilter(owner, filter);
    if (filterType == Filter.FILE_TYPE)
      return new DialogFileTypesFilter(owner, filter);
    if (filterType == Filter.HOST)
      return new DialogHostFilter(owner, filter);

    throw new IllegalArgumentException("There is no dialog for this filter: " + filter);
  }
}

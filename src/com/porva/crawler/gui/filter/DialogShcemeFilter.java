/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui.filter;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

import com.porva.crawler.filter.Filter;
import com.porva.crawler.filter.FilterFactory;
import com.porva.util.gui.CheckNode;
import com.porva.util.gui.CheckRenderer;
import com.porva.util.gui.JDialogStandart;
import com.porva.util.gui.NodeSelectionListener;

/**
 * GUI dialog to specify allowed schemes (protoclos) to construct {@link SchemeFilter} object.
 */
class DialogShcemeFilter extends DialogFileTypesFilterGUI
{
  private static final long serialVersionUID = -8808787807472524760L;

  private Filter filter;

  /**
   * Array of supported protocols.
   */
  private static final String[] supportedProtocols = new String[] { "http" };

  private String[] checkedProtocols;

  DialogShcemeFilter(final Frame owner, Filter filter)
  {
    super(owner);
    super.setModal(true);
    super.setTitle("Scheme filter");
    this.filter = filter;

    if (this.filter == null)
      throw new IllegalArgumentException("filter");

    pack();
    JDialogStandart.centerInParent(this);

    checkedProtocols = filter.getConfigStr().split(",");

    CheckNode top = new CheckNode("All supported protocols");
    for (String supportedProtocol : supportedProtocols) {
      CheckNode checkNode = new CheckNode(supportedProtocol);
      checkNode.setSelected(isChecked(supportedProtocol));
      top.add(checkNode);
    }

    DefaultTreeModel defaultTreeModel = new DefaultTreeModel(top);
    jTreeFileTypes.setModel(defaultTreeModel);
    jTreeFileTypes.setCellRenderer(new CheckRenderer());
    jTreeFileTypes.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    jTreeFileTypes.putClientProperty("JTree.lineStyle", "Angled");
    jTreeFileTypes.addMouseListener(new NodeSelectionListener(jTreeFileTypes));
    
    radioButtonDisallow.setEnabled(false);
    radioButtonAllow.setEnabled(true);
    radioButtonAllow.setSelected(true);
    labelFileTypes.setText("Select allowed protocols:");
    labelHelpTip.setText(""); // todo: 

    okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonOk_actionPerformed(e);
      }
    });
    getRootPane().setDefaultButton(okButton);

    cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonCancel_actionPerformed(e);
      }
    });
    radioButtonAllow.setMnemonic(KeyEvent.VK_A);

    AbstractAction cancelAction = new AbstractAction()
    {
      private static final long serialVersionUID = -6571333581511840527L;

      public void actionPerformed(ActionEvent e)
      {
        jButtonCancel_actionPerformed(e);
      }
    };
    JDialogStandart.addCancelByEscapeKey(this.getRootPane(), cancelAction);
  }

  private void getSelectedLeafNodesStr(CheckNode node, List<String> list)
  {
    if (node.isLeaf() && node.isSelected())
      list.add(node.toString());

    for (Enumeration en = node.children(); en.hasMoreElements();) {
      getSelectedLeafNodesStr((CheckNode) en.nextElement(), list);
    }
  }

  private boolean isChecked(final String protocol)
  {
    for (String prot : checkedProtocols)
      if (protocol.equalsIgnoreCase(prot))
        return true;
    return false;
  }

  /**
   * Button 'Ok' action. Constructs a new ProtocolFilter.
   * 
   * @param e
   */
  public void jButtonOk_actionPerformed(ActionEvent e)
  {
    List<String> checkedNodes = new ArrayList<String>();
    getSelectedLeafNodesStr((CheckNode) jTreeFileTypes.getModel().getRoot(), checkedNodes);
    String[] allowedProtocols = new String[checkedNodes.size()];
    checkedNodes.toArray(allowedProtocols);

    try {
      FilterFactory.initProtocolFilter(filter, allowedProtocols);
    } catch (Exception e1) {
      JOptionPane.showMessageDialog(this, "Select at least one protocol", "Shceme filter error",
                                    JOptionPane.ERROR_MESSAGE);
      return;
    }

    dispose();
  }

  /**
   * Cancel action.
   * 
   * @param e
   */
  public void jButtonCancel_actionPerformed(ActionEvent e)
  {
    dispose();
  }

}

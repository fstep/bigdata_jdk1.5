/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.CrawlerInit;
import com.porva.util.gui.JDialogStandart;
import com.porva.util.gui.OpenFileFilter;

/**
 * "New Project". A GUI dialog to create a new project.
 */
class DialogNewProject extends DialogNewProjectGUI
{

  private static final long serialVersionUID = 5217315668193067587L;

  private final MainFrame parentFrame;

  private final String projectName = "new_project";

  /**
   * Creates a new "New Project" dialog.
   * 
   * @param owner the main frame of the crawler.
   * @throws NullArgumentException if <code>owner</code> is <code>null</code>.
   */
  public DialogNewProject(final MainFrame owner)
  {
    super(owner);
    super.setModal(true);
    super.setTitle("New Project");
    parentFrame = owner;
    if (owner == null)
      throw new NullArgumentException("parentFrame");

    jTextFieldProjectName.setDocument(new PathChangerDocument());
    jTextFieldProjectDir.setDocument(new ProjectPathChangerDocument());
    jTextFieldProjectName.setText(projectName);
    jTextFieldProjectDir.setText(System.getProperty("user.dir") + File.separator + "projects"
        + File.separator + projectName + File.separator);
    setPaths(projectName);

    pack();
    JDialogStandart.centerInParent(this);

    okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonOk_actionPerformed(e);
      }
    });
    getRootPane().setDefaultButton(okButton);

    cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonCancel_actionPerformed(e);
      }
    });

    buttonProjectDir.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        buttonProjectDir_actionPerformed(e);
      }
    });

    buttonProjectFile.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        buttonProjectFile_actionPerformed(e);
      }
    });

    buttonDbDir.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        buttonDbDir_actionPerformed(e);
      }
    });

    buttonTaskFile.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        buttonTaskFile_actionPerformed(e);
      }
    });
    
    AbstractAction cancelAction = new AbstractAction()
    {
      private static final long serialVersionUID = -6571333581511840527L;

      public void actionPerformed(ActionEvent e)
      {
        jButtonCancel_actionPerformed(e);
      }
    };
    JDialogStandart.addCancelByEscapeKey(this.getRootPane(), cancelAction);

  }
  
  private void buttonProjectDir_actionPerformed(ActionEvent e)
  {
    JFileChooser chooser = new JFileChooser();
    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
      String projectDir = new String(chooser.getSelectedFile().toString());
      if (!projectDir.endsWith(File.separator))
        projectDir += File.separator;
      jTextFieldProjectDir.setText(projectDir);
    }
  }

  private void buttonProjectFile_actionPerformed(ActionEvent e)
  {
    JFileChooser chooser = new JFileChooser();
    chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    chooser.setFileFilter(new OpenFileFilter("crp"));
    if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
      String projectFileName = new String(chooser.getSelectedFile().toString());
      jTextFieldProjectFile.setText(projectFileName);
    }
  }

  private void buttonDbDir_actionPerformed(ActionEvent e)
  {
    JFileChooser chooser = new JFileChooser();
    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
      String dbDir = new String(chooser.getSelectedFile().toString());
      if (!dbDir.endsWith(File.separator))
        dbDir += File.separator;
      jTextFieldDbDir.setText(dbDir);
    }
  }

  private void buttonTaskFile_actionPerformed(ActionEvent e)
  {
    JFileChooser chooser = new JFileChooser();
    chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
      String waitingFile = new String(chooser.getSelectedFile().toString());
      jTextFieldWaitingFile.setText(waitingFile);
    }

  }

  private void jButtonCancel_actionPerformed(ActionEvent e)
  {
    this.dispose();
  }

  private void jButtonOk_actionPerformed(ActionEvent e)
  {
    String projectName = jTextFieldProjectName.getText();
    String projectFile = jTextFieldProjectFile.getText();
    String dbDir = jTextFieldDbDir.getText();
    String waitingFileStr = jTextFieldWaitingFile.getText();

    String dir = System.getProperty("user.dir") + File.separator + "projects" + File.separator
        + projectName + File.separator;
    File dirFile = new File(dir);
    if (!dirFile.mkdirs()) {
      if (dirFile.exists()) {
        JOptionPane.showMessageDialog(parentFrame, "Project directory already exists: " + dir,
                                      "New project error", JOptionPane.ERROR_MESSAGE);
        return;
      }

      JOptionPane.showMessageDialog(parentFrame, "Cannot create project directory: " + dir,
                                    "Create directory error", JOptionPane.ERROR_MESSAGE);
      return;
    }

    File configFile = new File(projectFile);
    if (!configFile.exists())
      try {
        configFile.createNewFile();
      } catch (IOException e1) {
        JOptionPane.showMessageDialog(parentFrame, "Cannot project configuration file: "
            + projectFile, "Create file error", JOptionPane.ERROR_MESSAGE);
        return;
      }

    File waitingFile = new File(waitingFileStr);
    if (!waitingFile.exists())
      try {
        waitingFile.createNewFile();
      } catch (IOException e1) {
        JOptionPane.showMessageDialog(parentFrame, "Cannot create file for initial tasks:" + " "
            + waitingFile, "Create file error", JOptionPane.ERROR_MESSAGE);
        return;
      }

    CrawlerInit.getProperties().putAll(CrawlerInit.getDefaultProperties());
    CrawlerInit.getProperties().put("project.name", projectName);
    CrawlerInit.getProperties().put("project.file", projectFile);
    CrawlerInit.getProperties().put("db.dir", dbDir);
    CrawlerInit.getProperties().put("workload.waiting.filename", waitingFileStr);

    this.dispose();

    DialogSettings dialogNewProjectCurrent = new DialogSettingsCurrent(parentFrame, true);
    dialogNewProjectCurrent.setSize(415, 525);
    // dialogNewProjectCurrent.setResizable(false);
    dialogNewProjectCurrent.setVisible(true);
  }

  private void setPaths(String projectName)
  {
    String dir = jTextFieldProjectDir.getText();

    Pattern p = null;
    if (File.separator.equals("\\"))
      p = Pattern.compile("^(.+)\\\\([^\\\\]*)\\\\$");
    else
      p = Pattern.compile("^(.+)" + File.separator + "([^" + File.separator + "]*)"
          + File.separator + "$");

    Matcher m = p.matcher(dir);
    if (m.matches()) {
      // System.out.println(m.group(0));
      // System.out.println(m.group(1));
      // System.out.println(m.group(2));
      dir = m.group(1) + File.separator + projectName + File.separator;
    }

    jTextFieldProjectDir.setText(dir);
    jTextFieldProjectFile.setText(dir + projectName + ".crp");
    jTextFieldDbDir.setText(dir + "db");
    jTextFieldWaitingFile.setText(dir + "waiting.crl");
  }

  // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  class PathChangerDocument extends PlainDocument
  {
    private static final long serialVersionUID = -2672666601105813990L;

    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException
    {
      if (str == null || str.equals(" ")) {
        return;
      }
      super.insertString(offs, str, a);
      String path = jTextFieldProjectName.getText();
      setPaths(path);
    }

    public void remove(int off, int len) throws BadLocationException
    {
      String removedStr = this.getText(off, len);

      if (removedStr == null || removedStr.length() == 0) {
        return;
      }

      super.remove(off, len);
      String path = jTextFieldProjectName.getText();
      setPaths(path);
    }
  }

  class ProjectPathChangerDocument extends PlainDocument
  {
    private static final long serialVersionUID = -8158793955488415717L;

    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException
    {
      if (str == null || str.equals(" ")) {
        return;
      }
      super.insertString(offs, str, a);
      setPaths();
    }

    public void remove(int off, int len) throws BadLocationException
    {
      String removedStr = this.getText(off, len);

      if (removedStr == null || removedStr.length() == 0) {
        return;
      }

      super.remove(off, len);
      setPaths();
    }

    private void setPaths()
    {
      String projectName = jTextFieldProjectName.getText();
      String dir = jTextFieldProjectDir.getText();
      jTextFieldProjectFile.setText(dir + projectName + ".crp");
      jTextFieldDbDir.setText(dir + "db");
      jTextFieldWaitingFile.setText(dir + "waiting.crl");
    }
  }

}
